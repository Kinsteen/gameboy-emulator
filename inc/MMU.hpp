#pragma once

#include <cstddef>
#include <cstdint>

class GameBoy;
class MBC;

class MMU {
  private:
    GameBoy *gb;

    uint8_t memory[64 * 1024];

    uint8_t cartridgeType; // As in the header in ROM
    uint8_t romSize;
    uint8_t ramSize;

    uint8_t *romFile = NULL;
    long romFileSize = 0;

    MBC *mbc = NULL;

  public:
    MMU(GameBoy *gb);
    ~MMU();

    void loadBootrom(const char *fileName);
    void loadRomInRAM(const char *fileName);
    void turnOffBootrom();
    void restoreRAM(const char *fileName);
    uint8_t *getRom();
    uint8_t *getRam();
    long getRomSize();
    long getRamSize();

    uint8_t readByte(uint16_t addr, bool force = false);  // 8 bits data
    uint16_t readWord(uint16_t addr, bool force = false); // 16 bits data

    void writeByte(uint16_t addr, uint8_t data, bool log = true, bool force = false);  // 8 bits data
    void writeWord(uint16_t addr, uint16_t data, bool log = true, bool force = false); // 16 bits data

    uint8_t *getPointerToByte(uint16_t addr); // Get pointer to byte in memory array
    void save(const char *fileName);
};