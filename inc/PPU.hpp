#pragma once

#include <cstdint>

#include "GameBoy.hpp"

class MMU;
class Debugger;

struct Sprite {
    int x;
    int y;
    int tileNumber;
    bool prio;
    bool XFlip;
    bool YFlip;
    uint8_t paletteData;
    uint16_t dataAddr;
};

struct Line {
    Pixel pixels[8]; // Left to right
    uint8_t y;       // Ranging from 0 to 7.
};

struct Tile {
    uint16_t address; // Start address where the tile is stored. It's an ABSOLUTE address.
    Line lines[8];    // Top to bottom
};

enum State {
    OAMSearch,
    PixelTransfer,
    HBlank,
    VBlank
};

class PPU {
  private:
    GameBoy *gb;
    CPU *cpu;
    MMU *mmu;
    Debugger *debugger;

    uint8_t oldIntState;

    bool displayEnable;

    State state;

    Pixel screen[160 * 144];
    uint32_t *byteScreen;

    uint8_t LY;
    int WILC; // Window Internal Line counter
    bool drewWindow;

    uint32_t ticks;
    uint16_t x;

    bool redraw;
    int sizeOfPixels;

    bool VBlankInt;
    bool StatInterrupt;

    int temp = 0;

    int spritesIndex = 0;
    struct Sprite frameSprites[40];

    int tilesReadInLine = 0;

  public:
    PPU(GameBoy *gb, CPU *cpu, MMU *mmu, Debugger *debugger);
    ~PPU();

    uint8_t scrollX;
    uint8_t scrollY;

    Color mapPalette(uint8_t palette, uint8_t raw_color);

    void tick(unsigned int cycles);

    void turnOffDisplay();

    void pushPixel(int x, int y, Pixel pixel);

    void printBg();

    uint8_t readControl(uint8_t bit);
    uint8_t readStat(uint8_t bit);

    uint32_t *createByteScreen(int sizeOfPixels);
    State getState();
    Pixel *getScreen();
    uint32_t *getByteScreen();
    bool needsRedraw();
    int getLY();
    int getInterrupts();
};
