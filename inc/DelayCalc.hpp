#pragma once
#include <bits/types/struct_timespec.h>
#include <unistd.h>

class DelayCalc {
  private:
    struct timespec startTime;
    struct timespec end;

    char name[256];
    int usThreshold;
    double delay = -1;

    double sum = 0;
    int n = 0;

    double calcDelta();

  public:
    DelayCalc(const char *name, int usThreshold = 100);
    ~DelayCalc();

    void add();
    double mean();
    void print();
    double get();
    void save();
    void start();
};