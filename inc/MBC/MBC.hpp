#pragma once

#include <cstddef>
#include <cstdint>

class MBC {
  protected:
    uint8_t cartridgeType;
    uint8_t romSize;
    uint8_t ramSize;

    uint8_t *ramContent = NULL;
    long ramFileSize = 0;
    bool ramEnabled = false;
    uint8_t selectedRomBank = 1;
    uint8_t selectedRamBank = 0;

  public:
    MBC(uint8_t cartridgeType, uint8_t romSize, uint8_t ramSize);
    ~MBC();

    virtual void handleCalls(uint16_t addr, uint8_t data) = 0;

    virtual void writeByteRam(uint16_t addr, uint8_t data) = 0;
    virtual uint8_t readByteRam(uint16_t addr) = 0;
    virtual uint8_t *getPointerToRam(uint16_t addr) = 0;
    virtual uint8_t getRomBankNumber() = 0;
    virtual void restoreRAM(const char *fileName) = 0;
    virtual void saveRAM(const char *fileName) = 0;
};