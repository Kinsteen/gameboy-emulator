#pragma once

#include "MBC.hpp"

class MBC1 : public MBC {
  private:
  public:
    MBC1(uint8_t cartridgeType, uint8_t romSize, uint8_t ramSize);
    ~MBC1();

    void handleCalls(uint16_t addr, uint8_t data);
    void writeByteRam(uint16_t addr, uint8_t data);
    uint8_t readByteRam(uint16_t addr);
    uint8_t *getPointerToRam(uint16_t addr);
    uint8_t getRomBankNumber();
    void restoreRAM(const char *fileName);
    void saveRAM(const char *fileName);
};