#pragma once

#include "GameBoy.hpp"

int stop();
unsigned int *getKeyBuffer();
void drawPixel(Pixel pixel, int x, int y);
