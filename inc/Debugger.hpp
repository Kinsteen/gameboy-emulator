#pragma once

#include <cstdint>

class MMU;

class Debugger {
  private:
    MMU *mmu;
    uint16_t breakpoints[32] = {0}; // 0 = no breakpoints

    int verbose;
    bool pause;
    bool stepByStep;
    bool forceTick;

  public:
    Debugger(MMU *mmu);
    ~Debugger();

    bool addBreakpoint(uint16_t addr);
    bool checkBreakpoints(uint16_t currentAddr);
    void printDebug(uint16_t programCounter);
    void printDebug(uint16_t programCounter, bool verbose);

    void printStack();
    void printOAM();
    void printVRAM();
    void printMemory(uint16_t start, uint16_t end);

    int getVerbose();
    bool cpuVerbose();
    bool ppuVerbose();
    bool otherVerbose();
    bool isPaused();
    bool isStepByStep();

    void toggleVerbose();
    void togglePause();
    void toggleStepByStep();

    bool getForcetick();
    void resetForcetick();

    void setVerbose(bool verbose);
    void setPause(bool pause);
    void setStepByStep(bool stepByStep);
};