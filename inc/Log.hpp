#pragma once

class Log {
  public:
    Log();
    ~Log();

    static void log(bool debug, const char *message, ...);
    static void log(const char *message, ...);
};