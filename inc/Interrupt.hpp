#pragma once

class GameBoy;
class CPU;
class MMU;
class Debugger;

enum InterruptType {
    IntVBlank,
    IntLCDStat,
    IntTimer,
    IntSerial,
    IntJoypad
};

class Interrupt {
  private:
    GameBoy *gb;
    CPU *cpu;
    MMU *mmu;

  public:
    Interrupt(GameBoy *gb, CPU *cpu, MMU *mmu);
    ~Interrupt();

    void requestInterrupt(enum InterruptType type);
    int checkInterrupt();
};