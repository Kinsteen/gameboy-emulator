#pragma once

#include <cstdint>

class CPU;
class MMU;
class PPU;
class Debugger;

enum PixelType {
    Background,
    Window,
    Sprite
};

enum Color {
    White,
    Light,
    Dark,
    Black,
    Errcolor
};

struct Pixel {
    Color color;       // Color found after decoding color palette.
    PixelType type;    // Origin of pixel (will be used in fetcher for priority with transparency in sprites)
    uint8_t raw_color; // Color data coded on last two significant bits
};

class GameBoy {
  public:
    CPU *cpu;
    MMU *mmu;
    PPU *ppu;
    Debugger *debugger;

    uint8_t masterInterrupt;

    uint8_t display[160 * 144];
    char romToLoad[512];

  public:
    GameBoy();
    ~GameBoy();

    void printState();
    void saveGame();
    void setRomToLoad(const char *name);
    void loadBootrom(const char *fileName);
    void loadProgram(const char *fileName, int startOffset = 0, int startRead = 0, int size = -1);
    void loadBank(int id);

    void tick(int *inputs);

    uint32_t *createByteScreen(int sizeOfPixels);
    Pixel *getScreen();
    uint32_t *getByteScreen();
    bool needsDraw();
    void turnOffBootrom();

    void setJoypad(int *inputs);

    uint8_t getCartridgeType();
};
