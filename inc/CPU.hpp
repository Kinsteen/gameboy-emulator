#pragma once

#include <cstdint>

class GameBoy;
class MMU;
class Debugger;
class Interrupt;

class Flag {
public:
    const static uint8_t Zero = 7;
    const static uint8_t Negative = 6;
    const static uint8_t HalfCarry = 5;
    const static uint8_t Carry = 4;
};

class CPU
{
private:
    GameBoy *gb;
    MMU *mmu;
    Debugger *debugger;
    Interrupt *interruptHandler;

    uint8_t opcode;

    int divCounter = 0;
    int timerDivider = 16;
    int timerTmp = 0;

public:
    struct { // AF
        union {
            struct {
                uint8_t F;
                uint8_t A;
            };
            uint16_t AF;
        };
    };
    
    struct { // BC
        union {
            struct {
                uint8_t C;
                uint8_t B;
            };
            uint16_t BC;
        };
    };
    
    struct { // DE
        union {
            struct {
                uint8_t E;
                uint8_t D;
            };
            uint16_t DE;
        };
    };

    struct { // HL
        union {
            struct {
                uint8_t L;
                uint8_t H;
            };
            uint16_t HL;
        };
    };

    typedef struct _instr {
        uint8_t x;
        uint8_t y;
        uint8_t z;
        uint8_t p;
        uint8_t q;
        uint8_t opcode;
    } Instruction;

    uint16_t programCounter = 0;
    uint16_t stackPointer = 0;
    bool cpuHalted = false;

    CPU(GameBoy *gb, Debugger *debugger);
    ~CPU();

    void setMMU(MMU *mmu);

    uint8_t *r(uint8_t index);
    void r(uint8_t index, uint8_t data);
    uint16_t *rp(uint8_t index);
    uint16_t *rp2(uint8_t index);

    void interrupt(uint16_t addr);

    int tick();

    void rotateLeft(uint8_t *reg);
    void rotateRight(uint8_t *reg);

    void     pushByteToStack(uint8_t  data);
    void     pushWordToStack(uint16_t data);
    uint8_t  popByteFromStack(); // 8 bits data
    uint16_t popWordFromStack(); // 16 bits data
    uint16_t readWordFromStack(uint16_t addr); // 16 bits data
    void     printStack();

    uint8_t getFlag(uint8_t flag);
    void setFlag(uint8_t flag, uint8_t value = 1); // By default, set the bit to 1
    void resetFlag(uint8_t flag);
    void resetAllFlags();

    // Get this in an ALU class ?

    bool calculateHalfCarry(uint16_t a, uint16_t b);
    void ALU(uint8_t y, uint8_t* z);

    int normalOpcode(CPU::Instruction instr);
    int CBOpcode(CPU::Instruction instr);

    Interrupt *getInterruptHandler();
    uint8_t getOpcode();
    uint16_t getPC(); // Returns actual program counter
    uint16_t getSP(); // Returns actual program counter
};
