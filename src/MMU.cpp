#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <memory>

#include "MMU.hpp"
#include "GameBoy.hpp"
#include "MBC/MBC.hpp"
#include "MBC/MBC1.hpp"
#include "MBC/MBC2.hpp"
#include "MBC/MBC3.hpp"
#include "Debugger.hpp"
#include "CPU.hpp"
#include "PPU.hpp"

MMU::MMU(GameBoy *gb) {
    printf("MMU INIT !\n");

    this->gb = gb;

    memset(memory, 0, sizeof(memory));
}

MMU::~MMU() {
    free(romFile);
}

void MMU::loadBootrom(const char *fileName) {
    FILE *file;
    int size = 0;
    file = fopen(fileName,"rb");  // r for read, b for binary

    fseek(file, 0, SEEK_END);
    size = ftell(file);
    fseek(file, 0, SEEK_SET);

    printf("Bootrom file has a size of %d bytes\n", size);

    fread(memory, size, 1, file);

    fclose(file);
}

void MMU::loadRomInRAM(const char *fileName) {
    FILE *file;
    int size = 0;
    file = fopen(fileName, "rb");  // r for read, b for binary

    fseek(file, 0, SEEK_END);
    size = ftell(file);
    fseek(file, 0, SEEK_SET);

    printf("Rom file has a size of %d bytes\n", size);
    this->romFileSize = size;

    this->romFile = (uint8_t *) malloc(sizeof(uint8_t)*size);

    int c = 0;
    int offset = 0;
    while ((c=getc(file)) != EOF) {
        romFile[offset] = c;
        offset++;
    }

    fclose(file);

    memcpy(this->memory+0x100, this->romFile+0x100, 0x8000-0x100);

    this->cartridgeType = memory[0x147];
    this->romSize = memory[0x148];
    this->ramSize = memory[0x149];

    printf("Cartridge type : %02X\n", this->cartridgeType);
    // Instanciate MBC ?
    // See https://gbdev.io/pandocs/#_0147-cartridge-type
    switch (this->cartridgeType) {
        case 0x0: {
            break;
        }

        case 0x01: { // MBC1
            printf("Ram size : %02X\n", ramSize);
            printf("Rom size : %02X\n", romSize);
            mbc = new MBC1(cartridgeType, romSize, ramSize);
            break;
        }

        case 0x02: { // MBC1+RAM
            printf("Ram size : %02X\n", ramSize);
            printf("Rom size : %02X\n", romSize);
            mbc = new MBC1(cartridgeType, romSize, ramSize);
            break;
        }

        case 0x03: { // MBC1+RAM+BATTERY
            printf("Ram size : %02X\n", ramSize);
            printf("Rom size : %02X\n", romSize);
            mbc = new MBC1(cartridgeType, romSize, ramSize);
            break;
        }

        case 0x06: { // MBC2+BATTERY
            printf("Ram size : %02X\n", ramSize);
            printf("Rom size : %02X\n", romSize);
            mbc = new MBC2(cartridgeType, romSize, ramSize);
            break;
        }

        case 0x13: { // MBC3+RAM+BATTERY
            mbc = new MBC3(cartridgeType, romSize, ramSize);
            break;
        }
        
        default:
            fprintf(stderr, "MBC (id : %02X) is not supported, can't execute the ROM.\n", this->cartridgeType);
            exit(-1);
            break;
    }
}

void MMU::turnOffBootrom() {
    memcpy(this->memory, this->romFile, 0x100);
}

void MMU::restoreRAM(const char *fileName) {
    if (mbc != NULL) {
        mbc->restoreRAM(fileName);
    }
}

uint8_t *MMU::getRom() {
    return this->romFile;
}

long MMU::getRomSize() {
    return this->romFileSize;
}

/**
 * Readers
 */
uint8_t MMU::readByte(uint16_t addr, bool force) {
    // if ((addr >= 0xFE00 && addr <= 0xFE9F && (gb->ppu->getState() != HBlank && gb->ppu->getState() != VBlank)) && !force) {
    //     return 0xFF;
    // }
    
    // if ((addr >= 0x8000 && addr <= 0x9FFF) && gb->ppu->getState() == PixelTransfer && !force) {
    //     return 0xFF;
    // }

    switch (addr) {
        case 0x4000 ... 0x7FFF: {
            if (mbc == NULL) {
                return memory[addr];
            } else {
                uint32_t newAddr = addr + 0x4000*(mbc->getRomBankNumber()-1);
                return romFile[newAddr];
            }
        }

        case 0xA000 ... 0xBFFF: {
            if (mbc != NULL) {
                return mbc->readByteRam(addr);
            } else {
                return memory[addr];
            }
        }

        default: {
            return memory[addr];
        }
    }
}

uint16_t MMU::readWord(uint16_t addr, bool force) {
    if (addr >= 0x0000 && (addr + 1) <= 0xFFFF) // Divide the memory into banks for reading to block reading when not authorized
        return readByte(addr + 1, force) << 8 | readByte(addr, force);
    
    return 0x0;
}

/**
 * Writers
 */
void MMU::writeByte(uint16_t addr, uint8_t data, bool log, bool force) {
    if (addr < 0x8000 && !force) {
        if (mbc != NULL) {
            mbc->handleCalls(addr, data);
            return;
        } else {
            return;
        }
    }

    if (addr == 0xFF04 && force == false) {
        memory[addr] = 0x0;
        return;
    }

    if (addr == 0xFF46) { // TODO : do DMA transfer in CPU.cpp, because it takes cycles
    // This copy needs 160 × 4 + 4 clocks to complete in both double speed and single speeds modes. The copy starts after the 4 setup clocks, and a new byte is copied every 4 clocks.
        uint16_t nAddr = data << 8;

        for (int i = 0; i <= 0x9F; i++) {
            this->writeByte(0xFE00 + i, this->readByte(nAddr + i), false, true);
        }
    }

    if (addr >= 0xA000 && addr < 0xC000) {
        if (mbc != NULL) {
            mbc->writeByteRam(addr, data);
        }

        return;
    }

    if (addr >= 0x8000 || force) {
        if ((addr >= 0xFE00 && addr <= 0xFE9F && (gb->ppu->getState() != HBlank && gb->ppu->getState() != VBlank)) && !force) {
            return;
        }

        if ((addr >= 0x8000 && addr <= 0x9FFF) && gb->ppu->getState() == PixelTransfer && !force) {
            //return; // This does regression on bootrom, logo doesn't appear correct
        }

        memory[addr] = data;
    }
}

void MMU::writeWord(uint16_t addr, uint16_t data, bool log, bool force) {
    uint8_t highByte = data >> 8;
    uint8_t lowByte = data & 0xFF;
    writeByte(addr, lowByte, log, force);
    writeByte(addr + 1, highByte, log, force);
}

uint8_t *MMU::getPointerToByte(uint16_t addr) {
    switch (addr) {
        case 0x4000 ... 0x7FFF: {
            if (mbc == NULL) {
                return &romFile[addr];
            } else {
                uint32_t newAddr = addr-0x4000 + 0x4000*(mbc->getRomBankNumber());
                return &romFile[newAddr];
            }
        }

        case 0xA000 ... 0xBFFF: {
            if (mbc != NULL) {
                return mbc->getPointerToRam(addr);
            } else {
                return &memory[addr];
            }
        }

        default: {
            return &memory[addr];
        }
    }
}

void MMU::save(const char *fileName) {
    if (mbc != NULL) {
        this->mbc->saveRAM(fileName);
    }
}
