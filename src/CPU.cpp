#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <memory>

#include "CPU.hpp"
#include "Debugger.hpp"
#include "GameBoy.hpp"
#include "Interrupt.hpp"
#include "Log.hpp"
#include "MMU.hpp"

CPU::CPU(GameBoy *gb, Debugger *debugger) {
    printf("CPU INIT\n");
    this->gb = gb;
    this->mmu = NULL;
    this->debugger = debugger;
}

CPU::~CPU() {
}

/**
 * The MMU pointer must be set after the constructor, because it's initialised after the MMU.
 */
void CPU::setMMU(MMU *mmu) {
    this->mmu = mmu;
    printf("CPU : MMU SET\n");
    this->interruptHandler = new Interrupt(gb, this, mmu);
}

uint8_t *CPU::r(uint8_t index) {
    // Used to get registers from opcodes
    uint8_t *r[8] = {
        &this->B,
        &this->C,
        &this->D,
        &this->E,
        &this->H,
        &this->L,
        this->mmu->getPointerToByte(this->HL),
        &this->A,
    };

    return r[index];
}

void CPU::r(uint8_t index, uint8_t data) {
    // Used to get registers from opcodes
    uint8_t *r[8] = {
        &this->B,
        &this->C,
        &this->D,
        &this->E,
        &this->H,
        &this->L,
        NULL,
        &this->A,
    };

    if (index == 6) {
        this->mmu->writeByte(this->HL, data);
    } else {
        *r[index] = data;
    }
}

uint16_t *CPU::rp(uint8_t index) {
    uint16_t *rp[4] = {
        &this->BC,
        &this->DE,
        &this->HL,
        &this->stackPointer,
    };

    return rp[index];
}

uint16_t *CPU::rp2(uint8_t index) {
    uint16_t *rp[4] = {
        &this->BC,
        &this->DE,
        &this->HL,
        &this->AF,
    };

    return rp[index];
}

void CPU::interrupt(uint16_t addr) {
    gb->masterInterrupt = 0;
    this->pushWordToStack(this->programCounter);
    this->programCounter = addr;
}

CPU::Instruction decodeOpcode(uint8_t opcode) {
    uint8_t x = opcode >> 6;
    uint8_t y = (opcode >> 3) & 0b111;
    uint8_t z = opcode & 0b111;
    uint8_t p = (y & 0b110) >> 1;
    uint8_t q = y & 0x001;

    return {x, y, z, p, q, opcode};
}

int CPU::tick() {
    int cycles = 4; // default ?

    if (this->interruptHandler->checkInterrupt() == 1) {
        cycles += 5;
    }

    // Fetch opcode
    uint8_t opcode = mmu->readByte(this->programCounter);
    this->opcode = opcode;

    if (gb->masterInterrupt >= 2)
        gb->masterInterrupt -= 2;

    if (this->cpuHalted == false) {
        if (opcode == 0xCB) {
            this->programCounter++;
            uint8_t opcode = mmu->readByte(this->programCounter);
            this->opcode = opcode;
            cycles = CBOpcode(decodeOpcode(opcode));
        } else {
            cycles = normalOpcode(decodeOpcode(opcode));
        }

        if (mmu->readByte(0xFF50) == 1) {
            this->gb->turnOffBootrom();
            mmu->writeByte(0xFF50, 0);
        }
    }

    this->divCounter += cycles;

    if (divCounter >= 256) {
        uint8_t oldDiv = mmu->readByte(0xFF04);
        mmu->writeByte(0xFF04, oldDiv + 1, false, true); // Bypass protection
        divCounter = divCounter - 256;
    }

    uint8_t timerControl = mmu->readByte(0xFF07);
    if ((timerControl >> 2 & 0x1) == 0x1) {
        timerTmp += cycles;

        switch (timerControl & 0x3) {
            case 0x0:
                this->timerDivider = 1024;
                break;
            case 0x1:
                this->timerDivider = 16;
                break;
            case 0x2:
                this->timerDivider = 64;
                break;
            case 0x3:
                this->timerDivider = 256;
                break;
        }

        if (this->timerTmp >= timerDivider) {
            uint8_t actual = mmu->readByte(0xFF05);
            if (actual == 0xFF) {
                interruptHandler->requestInterrupt(IntTimer);
                uint8_t timMod = mmu->readByte(0xFF06);
                mmu->writeByte(0xFF05, timMod, false, true);
            } else {
                mmu->writeByte(0xFF05, actual + 1, false, true);
            }

            this->timerTmp = this->timerTmp - timerDivider;
        }
    }

    return cycles;
}

/**
 * Pushers
 */
void CPU::pushByteToStack(uint8_t data) {
    Log::log(debugger->cpuVerbose(), "Stack pointer before push : %04X // Data to push : %02X\n", this->stackPointer, data);
    this->stackPointer--;
    mmu->writeByte(this->stackPointer, data, false, true);
}

void CPU::pushWordToStack(uint16_t data) {
    pushByteToStack(data >> 8);
    pushByteToStack(data & 0xFF);
    Log::log(debugger->cpuVerbose(), "Pushed %04X to stack\n", data);
}

/**
 * Poppers
 */
uint8_t CPU::popByteFromStack() {
    uint8_t data = mmu->readByte(this->stackPointer);
    this->stackPointer++;
    return data;
}

uint16_t CPU::popWordFromStack() {
    uint16_t data = readWordFromStack(this->stackPointer);
    Log::log(debugger->cpuVerbose(), "Popped %04X to stack\n", data);
    this->stackPointer += 2;
    return data;
}

uint16_t CPU::readWordFromStack(uint16_t addr) {
    uint16_t data = (mmu->readByte(addr + 1) << 8) | mmu->readByte(addr);
    Log::log(debugger->cpuVerbose(), "Addr : %04X / Data : %04X\n", addr, data);
    return data;
}

uint8_t CPU::getFlag(uint8_t flag) {
    return (this->F >> flag) & 0x1;
}

void CPU::setFlag(uint8_t flag, uint8_t value) {
    if (value) {
        uint8_t mask = 1 << flag;
        this->F |= mask;
    } else {
        uint8_t mask = 1 << flag;
        this->F &= ~mask;
    }
}

void CPU::ALU(uint8_t y, uint8_t *z) {
    switch (y) { // This represent the operation
        case 0:  // ADD A, n (8 instructions)
            setFlag(Flag::HalfCarry, (((this->A & 0xF) + (*z & 0xF)) & 0x10) == 0x10);
            setFlag(Flag::Carry, this->A > 0xFF - *z);

            this->A += *z;

            setFlag(Flag::Zero, this->A == 0);
            setFlag(Flag::Negative, 0);
            break;

        case 1: { // ADC A, n
            int oldCarry = getFlag(Flag::Carry);
            setFlag(Flag::HalfCarry, (((this->A & 0xF) + (*z & 0xF) + oldCarry) & 0x10) == 0x10);
            setFlag(Flag::Carry, this->A > 0xFF - *z - oldCarry);

            this->A += *z + oldCarry;

            setFlag(Flag::Zero, this->A == 0);
            setFlag(Flag::Negative, 0);
            break;
        }

        case 2: // SUB n
            setFlag(Flag::HalfCarry, (this->A & 0xF) < (*z & 0xF));
            setFlag(Flag::Carry, *z > this->A);

            this->A -= *z;

            Log::log(debugger->cpuVerbose(), "SUB %d\n", *z);

            setFlag(Flag::Zero, this->A == 0);
            setFlag(Flag::Negative, 1);
            break;

        case 3: { // SBC A, n
            uint8_t oldCarry = getFlag(Flag::Carry);
            setFlag(Flag::HalfCarry, (this->A & 0xF) < (*z & 0xF) + oldCarry);
            setFlag(Flag::Carry, *z + oldCarry > this->A);

            this->A -= *z + oldCarry;

            setFlag(Flag::Zero, this->A == 0);
            setFlag(Flag::Negative, 1);
            break;
        }

        case 4: // AND n
            this->A &= *z;
            setFlag(Flag::Zero, this->A == 0);
            setFlag(Flag::Negative, 0);
            setFlag(Flag::HalfCarry, 1);
            setFlag(Flag::Carry, 0);
            break;

        case 5: // XOR n
            this->A = this->A ^ *z;

            setFlag(Flag::Zero, this->A == 0);
            setFlag(Flag::Negative, 0);
            setFlag(Flag::HalfCarry, 0);
            setFlag(Flag::Carry, 0);
            break;

        case 6: // OR n
            this->A = this->A | *z;

            setFlag(Flag::Zero, this->A == 0);
            setFlag(Flag::Negative, 0);
            setFlag(Flag::HalfCarry, 0);
            setFlag(Flag::Carry, 0);
            break;

        case 7: { // CP n
            uint8_t a = this->A;
            uint8_t b = *z;
            setFlag(Flag::Zero, this->A == b);
            setFlag(Flag::Negative, 1);
            setFlag(Flag::HalfCarry, (((a & 0xf) - (b & 0xf)) & 0x10) == 0x10);
            setFlag(Flag::Carry, this->A < b);
            break;
        }
    }
}

int CPU::normalOpcode(CPU::Instruction instr) {
    uint8_t opcode = instr.opcode;

    uint8_t x = instr.x;
    uint8_t y = instr.y;
    uint8_t z = instr.z;
    uint8_t p = instr.p;
    uint8_t q = instr.q;

    switch (x) {
        case 0: {
            switch (z) {
                case 0: {
                    switch (y) {
                        case 0: { // NOP
                            programCounter++;
                            return 4;
                        }

                        case 1: { // LD (nn), SP
                            uint16_t nn = mmu->readWord(this->programCounter + 1);
                            mmu->writeWord(nn, this->stackPointer);
                            this->programCounter += 3;
                            return 20;
                        }

                        case 2: { // STOP
                            this->cpuHalted = true;
                            this->programCounter++;
                            return 4;
                        }

                        case 3: {                                               // JR d
                            int8_t n = mmu->readByte(this->programCounter + 1); // n is signed !!
                            this->programCounter += n + 2;
                            return 8;
                        }

                        case 4: { // JR NZ, r8
                            int8_t n = mmu->readByte(this->programCounter + 1);

                            if (getFlag(Flag::Zero) == 0)
                                this->programCounter += n;

                            this->programCounter += 2;
                            return 8;
                        }
                        case 5: { // JR Z, r8
                            int8_t n = mmu->readByte(this->programCounter + 1);

                            if (getFlag(Flag::Zero) == 1)
                                this->programCounter += n;

                            this->programCounter += 2;
                            return 8;
                        }
                        case 6: { // JR NC, r8
                            int8_t n = mmu->readByte(this->programCounter + 1);

                            if (getFlag(Flag::Carry) == 0)
                                this->programCounter += n;

                            this->programCounter += 2;
                            return 8;
                        }
                        case 7: { // JR C, r8
                            int8_t n = mmu->readByte(this->programCounter + 1);

                            if (getFlag(Flag::Carry) == 1)
                                this->programCounter += n;

                            this->programCounter += 2;
                            return 8;
                        }
                    }
                    break;
                }

                case 1: {
                    if (q == 0) { // LD rp(p), nn
                        uint16_t nn = mmu->readWord(this->programCounter + 1);
                        Log::log(debugger->cpuVerbose(), "LD rp, nn: LD %d, %04X\n", p, nn);
                        *rp(p) = nn;
                        this->programCounter += 3;
                        return 12;
                    } else { // ADD HL, rp(p)
                        setFlag(Flag::Carry, this->HL > 0xFFFF - *rp(p));
                        setFlag(Flag::HalfCarry, (((this->HL & 0xFFF) + (*rp(p) & 0xFFF)) & 0x1000) == 0x1000);

                        Log::log(debugger->cpuVerbose(), "ADD HL, rp %d\n", p);
                        this->HL += *rp(p);

                        setFlag(Flag::Negative, 0);

                        this->programCounter++;
                        return 8;
                    }
                }

                case 2: {
                    switch (q) {
                        case 0: {
                            switch (p) {
                                case 0: // LD (BC), A
                                    Log::log(debugger->cpuVerbose(), "LD (BC), A\n");
                                    mmu->writeByte(this->BC, this->A);
                                    this->programCounter++;
                                    return 8;
                                case 1: // LD (DE), A
                                    Log::log(debugger->cpuVerbose(), "LD (DE), A\n");
                                    mmu->writeByte(this->DE, this->A);
                                    this->programCounter++;
                                    return 8;
                                case 2: // LD (HL+), A
                                    Log::log(debugger->cpuVerbose(), "LD (HL+),A: LD(%04X),%02X\n");
                                    mmu->writeByte(this->HL, this->A);
                                    this->HL++;
                                    Log::log(debugger->cpuVerbose(), "INC HL from to %04X\n", this->HL);
                                    this->programCounter++;
                                    return 8;
                                case 3: // LD (HL-), A
                                    Log::log(debugger->cpuVerbose(), "LD (HL-),A\n");
                                    mmu->writeByte(this->HL, this->A);
                                    this->HL--;
                                    this->programCounter++;
                                    return 8;
                            }
                            break;
                        }

                        case 1:
                            switch (p) {
                                case 0: // LD A, (BC)
                                    this->A = mmu->readByte(this->BC);
                                    this->programCounter++;
                                    return 8;
                                case 1: // LD A, (DE)
                                    Log::log(debugger->cpuVerbose(), "LD A, (DE) = LD A, (%04X) = ", this->DE);
                                    this->A = mmu->readByte(this->DE);
                                    Log::log(debugger->cpuVerbose(), "0x%02X\n", this->A);
                                    this->programCounter++;
                                    return 8;
                                case 2: // LD A, (HL+)
                                    this->A = mmu->readByte(this->HL);
                                    this->HL++;
                                    Log::log(debugger->cpuVerbose(), "HL inc\n");
                                    this->programCounter++;
                                    return 8;
                                case 3: // LD A, (HL-)
                                    this->A = mmu->readByte(this->HL);
                                    this->HL--;
                                    Log::log(debugger->cpuVerbose(), "HL dec\n");
                                    this->programCounter++;
                                    return 8;
                            }
                            break;
                    }
                    break;
                }

                case 3: {
                    if (q == 0) { // INC rp(p)
                        *rp(p) += 1;
                    } else { // DEC rp(p)
                        *rp(p) -= 1;
                    }
                    this->programCounter++;
                    return 8;
                }

                case 4: { // INC 8-bit
                    setFlag(Flag::HalfCarry, (((*r(y) & 0xF) + 1) & 0x10) == 0x10);
                    r(y, *r(y) + 1);
                    Log::log(debugger->cpuVerbose(), "INC to %x\n", *(r(y)));
                    setFlag(Flag::Zero, *(r(y)) == 0);
                    setFlag(Flag::Negative, 0);

                    this->programCounter++;
                    return y == 6 ? 12 : 4;
                }

                case 5: { // DEC 8-bit
                    setFlag(Flag::HalfCarry, ((*r(y) & 0xF) - 1) < 0);
                    r(y, *r(y) - 1);
                    setFlag(Flag::Zero, *r(y) == 0);
                    setFlag(Flag::Negative);
                    this->programCounter++;
                    return y == 6 ? 12 : 4;
                }

                case 6: { // LD r(y), n
                    int n = mmu->readByte(this->programCounter + 1);
                    r(y, n);
                    this->programCounter += 2;
                    return 8;
                }

                case 7: {
                    switch (y) {
                        case 0: { // RLCA
                            uint8_t oldData = this->A >> 7;

                            this->A = (this->A << 1) | oldData;
                            setFlag(Flag::Carry, oldData);
                            setFlag(Flag::Zero, 0);
                            setFlag(Flag::Negative, 0);
                            setFlag(Flag::HalfCarry, 0);
                            this->programCounter++;
                            return 4;
                        }
                        case 1: { // RRCA
                            int oldData = A & 1;

                            this->A = (this->A >> 1) | (oldData << 7);
                            setFlag(Flag::Carry, oldData);
                            setFlag(Flag::Zero, 0);
                            setFlag(Flag::Negative, 0);
                            setFlag(Flag::HalfCarry, 0);
                            this->programCounter++;
                            return 4;
                        }
                        case 2: { // RLA
                            uint8_t leftOver = this->A >> 7;
                            uint8_t oldCarry = getFlag(Flag::Carry);

                            this->A = this->A << 1 | oldCarry;
                            setFlag(Flag::Zero, 0);
                            setFlag(Flag::Negative, 0);
                            setFlag(Flag::HalfCarry, 0);
                            setFlag(Flag::Carry, leftOver);
                            this->programCounter++;
                            return 4;
                        }
                        case 3: { // RRA
                            uint8_t leftOver = this->A & 1;
                            uint8_t oldCarry = getFlag(Flag::Carry);

                            this->A = this->A >> 1 | oldCarry << 7;
                            setFlag(Flag::Zero, 0);
                            setFlag(Flag::Negative, 0);
                            setFlag(Flag::HalfCarry, 0);
                            setFlag(Flag::Carry, leftOver);
                            this->programCounter++;
                            return 4;
                        }
                        case 4: { // DAA
                            int correction = 0;

                            if (getFlag(Flag::HalfCarry) || (!getFlag(Flag::Negative) && (A & 0xf) > 9)) {
                                correction |= 0x6;
                            }

                            if (getFlag(Flag::Carry) || (!getFlag(Flag::Negative) && A > 0x99)) {
                                correction |= 0x60;
                                setFlag(Flag::Carry, 1);
                            }

                            A += getFlag(Flag::Negative) ? -correction : correction;

                            setFlag(Flag::Zero, A == 0);
                            setFlag(Flag::HalfCarry, 0);

                            this->programCounter++;
                            return 4;
                        }
                        case 5: { // CPL
                            this->A ^= 0xFFFF;
                            setFlag(Flag::Negative, 1);
                            setFlag(Flag::HalfCarry, 1);
                            this->programCounter++;
                            return 4;
                        }
                        case 6: { // SCF
                            setFlag(Flag::Negative, 0);
                            setFlag(Flag::HalfCarry, 0);
                            setFlag(Flag::Carry, 1);
                            this->programCounter++;
                            return 4;
                        }
                        case 7: { // CCF
                            setFlag(Flag::Negative, 0);
                            setFlag(Flag::HalfCarry, 0);
                            setFlag(Flag::Carry, getFlag(Flag::Carry) == 1 ? 0 : 1);
                            this->programCounter++;
                            return 4;
                        }
                    }
                    break;
                }
            }
            break;
        }

        case 1: {
            if (z == 6 && y == 6) { // HALT
                this->cpuHalted = true;
                this->programCounter++;
                return 4;
            } else { // LD ry rz
                r(y, *r(z));
                this->programCounter++;

                if ((y == 6) || (z == 6)) {
                    return 8;
                } else {
                    return 4;
                }
            }
        }

        case 2: {
            ALU(y, r(z));
            this->programCounter++;

            return z == 6 ? 8 : 4;
        }

        case 3: { // x = 3
            switch (z) {
                case 0:
                    switch (y) {
                        case 0: { // RET NZ
                            if (getFlag(Flag::Zero) == 0) {
                                uint16_t popped = this->popWordFromStack();

                                this->programCounter = popped;
                            } else {
                                programCounter++;
                            }
                            return 8;
                        }
                        case 1: { // RET Z
                            if (getFlag(Flag::Zero) == 1) {
                                uint16_t popped = this->popWordFromStack();

                                this->programCounter = popped;
                            } else {
                                programCounter++;
                            }
                            return 8;
                        }
                        case 2: { // RET NC
                            if (getFlag(Flag::Carry) == 0) {
                                uint16_t popped = this->popWordFromStack();

                                this->programCounter = popped;
                            } else {
                                programCounter++;
                            }
                            return 8;
                        }
                        case 3: { // RET C
                            if (getFlag(Flag::Carry) == 1) {
                                uint16_t popped = this->popWordFromStack();

                                this->programCounter = popped;
                            } else {
                                programCounter++;
                            }
                            return 8;
                        }

                        case 4: { // LD (n+FF00), A
                            uint8_t n = mmu->readByte(this->programCounter + 1);
                            mmu->writeByte(n + 0xFF00, this->A);
                            this->programCounter += 2;
                            return 12;
                        }

                        case 5: { // ADD SP,n
                            int8_t n = mmu->readByte(this->programCounter + 1);
                            uint8_t un = n;

                            setFlag(Flag::Zero, 0);
                            setFlag(Flag::Negative, 0);
                            setFlag(Flag::HalfCarry, (((this->stackPointer & 0xF) + (un & 0xF)) & 0x10) == 0x10);
                            setFlag(Flag::Carry, (((this->stackPointer & 0xFF) + (un & 0xFF)) & 0x100) == 0x100);

                            this->stackPointer += n;
                            this->programCounter += 2;

                            return 16;
                        }
                        case 6: { // LDH A,(a8) = LD A,($FF00+a8)
                            uint8_t n = mmu->readByte(this->programCounter + 1);
                            this->A = mmu->readByte(0xFF00 + n);
                            this->programCounter += 2;
                            return 12;
                        }
                        case 7: { // LD HL, SP+n
                            int8_t n = mmu->readByte(this->programCounter + 1);
                            uint8_t un = n;
                            setFlag(Flag::Zero, 0);
                            setFlag(Flag::Negative, 0);
                            setFlag(Flag::HalfCarry, (((this->stackPointer & 0xF) + (un & 0xF)) & 0x10) == 0x10);
                            setFlag(Flag::Carry, (((this->stackPointer & 0xFF) + (un & 0xFF)) & 0x100) == 0x100);

                            this->HL = this->stackPointer + n;
                            this->programCounter += 2;
                            return 12;
                        }
                    }
                    break;
                case 1: {
                    switch (q) {
                        case 0: { // POP rp2
                            if (p == 3) {
                                *rp2(p) = this->popWordFromStack() & 0xFFF0; // Hardwired F lower bits flags
                            } else {
                                *rp2(p) = this->popWordFromStack();
                            }
                            this->programCounter++;
                            return 12;
                        }
                        case 1: {
                            switch (p) {
                                case 0: { // RET
                                    uint16_t popped = this->popWordFromStack();

                                    this->programCounter = popped;
                                    return 8;
                                }
                                case 1: { // RETI
                                    uint16_t popped = this->popWordFromStack();

                                    this->programCounter = popped;
                                    gb->masterInterrupt = 5; // This will enable interrupts on next instruction
                                    return 8;
                                }
                                case 2: { // JP HL
                                    this->programCounter = this->HL;
                                    return 4;
                                }
                                case 3: { // LD SP,HL
                                    this->stackPointer = this->HL;
                                    this->programCounter++;
                                    return 8;
                                }
                            }
                        }
                    }
                    break;
                }
                case 2:
                    switch (y) {
                        case 0: { // JP NZ, nn
                            uint16_t nn = mmu->readWord(this->programCounter + 1);
                            if (getFlag(Flag::Zero) == 0) {
                                this->programCounter = nn;
                            } else {
                                this->programCounter += 3;
                            }
                            return 12;
                        }
                        case 1: { // JP Z, nn
                            uint16_t nn = mmu->readWord(this->programCounter + 1);
                            if (getFlag(Flag::Zero) == 1) {
                                this->programCounter = nn;
                            } else {
                                this->programCounter += 3;
                            }
                            return 12;
                        }
                        case 2: { // JP NC, nn
                            uint16_t nn = mmu->readWord(this->programCounter + 1);
                            if (getFlag(Flag::Carry) == 0) {
                                this->programCounter = nn;
                            } else {
                                this->programCounter += 3;
                            }
                            return 12;
                        }
                        case 3: { // JP C, nn
                            uint16_t nn = mmu->readWord(this->programCounter + 1);
                            if (getFlag(Flag::Carry) == 1) {
                                this->programCounter = nn;
                            } else {
                                this->programCounter += 3;
                            }
                            return 12;
                        }
                        case 4: { // LD (C+FF00), A
                            uint16_t addr = this->C + 0xFF00;
                            mmu->writeByte(addr, this->A);
                            this->programCounter++;
                            return 8;
                        }
                        case 5: {                                                  // LD (nn), A
                            uint16_t nn = mmu->readWord(this->programCounter + 1); // This will read 16 bits
                            mmu->writeByte(nn, this->A);
                            this->programCounter += 3;
                            return 16;
                        }
                        case 6: { // LD A, (C+FF00)
                            uint16_t addr = this->C + 0xFF00;
                            this->A = mmu->readByte(addr);
                            this->programCounter++;
                            return 8;
                        }
                        case 7: { // LD A, (nn)
                            uint16_t nn = mmu->readWord(this->programCounter + 1);
                            this->A = mmu->readByte(nn);
                            this->programCounter += 3;
                            return 16;
                        }
                    }
                    break;
                case 3: {
                    switch (y) {
                        case 0: { // JP nn
                            uint16_t nn = mmu->readWord(this->programCounter + 1);
                            this->programCounter = nn;
                            return 12;
                        }
                        case 6: { // DI
                            // Disables interrupts.
                            gb->masterInterrupt = 0;
                            this->programCounter++;
                            return 4;
                        }
                        case 7: { // EI
                            // Enables interrupts.
                            // Set as 3, because it'll be enabled on next instruction
                            gb->masterInterrupt = 5;
                            this->programCounter++;
                            return 4;
                        }

                        default:
                            fprintf(stderr, "%x at %x: %d, %d, %d not implemented\n", opcode, this->programCounter, x, y, z);
                            exit(EXIT_FAILURE);
                            break;
                    }
                    break;
                }

                case 4: {
                    switch (y) {
                        case 0: { // CALL NZ nn
                            if (getFlag(Flag::Zero) == 0) {
                                this->pushWordToStack(this->programCounter + 3);
                                uint16_t nn = mmu->readWord(this->programCounter + 1);
                                Log::log(this->debugger->cpuVerbose(), "CALL NZ %04X, from %04X\n", nn, this->programCounter);
                                this->programCounter = nn;
                            } else {
                                this->programCounter += 3;
                            }
                            break;
                        }

                        case 1: { // CALL Z nn
                            if (getFlag(Flag::Zero) == 1) {
                                this->pushWordToStack(this->programCounter + 3);
                                uint16_t nn = mmu->readWord(this->programCounter + 1);
                                Log::log(this->debugger->cpuVerbose(), "CALL Z %04X, from %04X\n", nn, this->programCounter);
                                this->programCounter = nn;
                            } else {
                                this->programCounter += 3;
                            }
                            break;
                        }

                        case 2: { // CALL NC nn
                            if (getFlag(Flag::Carry) == 0) {
                                this->pushWordToStack(this->programCounter + 3);
                                uint16_t nn = mmu->readWord(this->programCounter + 1);
                                Log::log(this->debugger->cpuVerbose(), "CALL NZ %04X, from %04X\n", nn, this->programCounter);
                                this->programCounter = nn;
                            } else {
                                this->programCounter += 3;
                            }
                            break;
                        }

                        case 3: { // CALL C nn
                            if (getFlag(Flag::Carry) == 1) {
                                this->pushWordToStack(this->programCounter + 3);
                                uint16_t nn = mmu->readWord(this->programCounter + 1);
                                Log::log(this->debugger->cpuVerbose(), "CALL NZ %04X, from %04X\n", nn, this->programCounter);
                                this->programCounter = nn;
                            } else {
                                this->programCounter += 3;
                            }
                            break;
                        }
                    }

                    return 12;
                }

                case 5: {
                    if (q == 0) {
                        // PUSH rp2[p]
                        this->pushWordToStack(*rp2(p));
                        this->programCounter++;
                        return 16;
                    } else if (q == 1 && p == 0) { // CALL nn
                        this->pushWordToStack(this->programCounter + 3);
                        uint16_t nn = mmu->readWord(this->programCounter + 1);
                        Log::log(this->debugger->cpuVerbose(), "CALL %04X, from %04X\n", nn, this->programCounter);
                        this->programCounter = nn;
                        return 12;
                    } else {
                        fprintf(stderr, "%x at %x: Unsupported instruction !\n", opcode, this->programCounter);
                        exit(EXIT_FAILURE);
                    }
                }

                case 6: { // ALU r(y), n
                    ALU(y, mmu->getPointerToByte(this->programCounter + 1));
                    this->programCounter += 2;
                    return 8;
                }

                case 7: { // RST y*8
                    this->pushWordToStack(this->programCounter + 1);
                    this->programCounter = 0x0 + y * 8;
                    return 32;
                }
            }
            break;
        }
    }

    return 0;
}

int CPU::CBOpcode(CPU::Instruction instr) {
    uint8_t x = instr.x;
    uint8_t y = instr.y;
    uint8_t z = instr.z;

    switch (x) {
        case 0: { // Rotations
            switch (y) {
                case 0: { // RLC
                    uint8_t oldData = *r(z) >> 7;

                    r(z, (*r(z) << 1) | oldData);

                    setFlag(Flag::Carry, oldData);
                    setFlag(Flag::Zero, *r(z) == 0);
                    setFlag(Flag::Negative, 0);
                    setFlag(Flag::HalfCarry, 0);
                    this->programCounter++;
                    break;
                }
                case 1: { // RRC
                    uint8_t oldData = *r(z) & 0x1;

                    r(z, (*r(z) >> 1) | (oldData << 7));

                    setFlag(Flag::Carry, oldData);
                    setFlag(Flag::Zero, *r(z) == 0);
                    setFlag(Flag::Negative, 0);
                    setFlag(Flag::HalfCarry, 0);
                    this->programCounter++;
                    break;
                }
                case 2: { // RL through carry flag
                    uint8_t leftOver = *r(z) >> 7;
                    uint8_t oldCarry = getFlag(Flag::Carry);

                    r(z, *r(z) << 1 | oldCarry);
                    setFlag(Flag::Zero, *r(z) == 0);
                    setFlag(Flag::Negative, 0);
                    setFlag(Flag::HalfCarry, 0);
                    setFlag(Flag::Carry, leftOver);
                    this->programCounter++;
                    break;
                }
                case 3: { // RR
                    uint8_t leftOver = *r(z) & 1;
                    uint8_t oldCarry = getFlag(Flag::Carry);

                    r(z, *r(z) >> 1 | oldCarry << 7);
                    setFlag(Flag::Zero, *r(z) == 0);
                    setFlag(Flag::Negative, 0);
                    setFlag(Flag::HalfCarry, 0);
                    setFlag(Flag::Carry, leftOver);
                    this->programCounter++;
                    break;
                }
                case 4: // SLA r(z)
                    setFlag(Flag::Carry, *r(z) >> 7);
                    r(z, *r(z) << 1);
                    setFlag(Flag::Zero, *r(z) == 0);
                    setFlag(Flag::Negative, 0);
                    setFlag(Flag::HalfCarry, 0);
                    this->programCounter++;
                    break;
                case 5: // SRA
                    setFlag(Flag::Carry, *r(z) & 0x1);
                    r(z, (*r(z) >> 1) | (*r(z) & 0b10000000));
                    setFlag(Flag::Zero, *r(z) == 0);
                    setFlag(Flag::Negative, 0);
                    setFlag(Flag::HalfCarry, 0);
                    this->programCounter++;
                    break;
                case 6: { // SWAP
                    uint8_t lower = *r(z) & 0x0F;
                    uint8_t upper = *r(z) & 0xF0;

                    r(z, (lower << 4) | (upper >> 4));

                    setFlag(Flag::Zero, *r(z) == 0);
                    setFlag(Flag::Negative, 0);
                    setFlag(Flag::HalfCarry, 0);
                    setFlag(Flag::Carry, 0);

                    this->programCounter++;
                    break;
                }
                case 7: { // SRL
                    uint8_t leftOver = *r(z) & 1;

                    r(z, *r(z) >> 1);
                    setFlag(Flag::Zero, *r(z) == 0);
                    setFlag(Flag::Negative, 0);
                    setFlag(Flag::HalfCarry, 0);
                    setFlag(Flag::Carry, leftOver);

                    this->programCounter++;
                    break;
                }
            }
            break;
        }

        case 1: { // BIT
            uint8_t bitToCheck = 0x1 << y;

            if ((bitToCheck & *r(z)) == bitToCheck) {
                setFlag(Flag::Zero, 0);
            } else {
                setFlag(Flag::Zero, 1);
            }

            setFlag(Flag::Negative, 0);
            setFlag(Flag::HalfCarry, 1);
            this->programCounter++;
            break;
        }

        case 2: { // RES
            r(z, *r(z) & ~(1 << y));
            this->programCounter++;
            break;
        }

        case 3: { // SET
            r(z, *r(z) | (1 << y));
            this->programCounter++;
            break;
        }
    }

    return z == 6 ? 16 : 8;
}

Interrupt *CPU::getInterruptHandler() {
    return this->interruptHandler;
}

uint8_t CPU::getOpcode() {
    return this->opcode;
}

uint16_t CPU::getPC() {
    return this->programCounter;
}

uint16_t CPU::getSP() {
    return this->stackPointer;
}
