#define BILLION 1E9

#include <cstdint>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <ctime>

#include "DelayCalc.hpp"

DelayCalc::DelayCalc(const char *name, int usThreshold) {
    strcpy(this->name, name);
    this->usThreshold = usThreshold;
    this->start();
}

DelayCalc::~DelayCalc()
{
}

void DelayCalc::add() {
    this->save();
    this->sum += this->delay;
    this->n++;
    this->start();
}

double DelayCalc::mean() {
    return this->sum/this->n;
}

void DelayCalc::print() {
    if (delay == -1)
        this->save();

    printf("Delay %s: %lf\n", this->name, this->delay);
}

double DelayCalc::get() {
    if (this->delay == -1)
        this->save();
    return this->delay;
}

void DelayCalc::save() {
    clock_gettime(CLOCK_MONOTONIC, &this->end);
    double delta = calcDelta();
    this->delay = delta;
}

void DelayCalc::start() {
    clock_gettime(CLOCK_MONOTONIC, &this->startTime);
}

double DelayCalc::calcDelta() {
    return (this->end.tv_sec + this->end.tv_nsec/BILLION) - (this->startTime.tv_sec + this->startTime.tv_nsec/BILLION);
}