#define BILLION 1E9

#include <cstdint>

#include <SDL2/SDL.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <ctime>
#include <thread>

#include "GameBoy.hpp"
#include "CPU.hpp"
#include "MMU.hpp"
#include "PPU.hpp"
#include "Debugger.hpp"
#include "DelayCalc.hpp"
#include "Log.hpp"
#include "main.hpp"

SDL_Window *window = NULL;
SDL_Renderer *renderer;
SDL_Surface *windowSurface = NULL, *surface = NULL;
SDL_Texture *texture;
uint8_t sizeOfPixels = 5;
int quit = 0;
unsigned int keyBuffer[16];
uint32_t *pixels;

int stop() {
    quit = 1;
    if(NULL != window)
        SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}

int main(int argc, char *argv[])
{
    int delay = 16;
    char fileToLoad[32];

    if (argc == 1) {
        SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
            "Please specify ROM to load",
            "You did not specify a ROM to load, you need to run the program with the ROM name in first argument.",
            NULL);
        exit(EXIT_FAILURE);
    } else if (argc == 3) {
        sizeOfPixels = atoi(argv[2]);
        printf("New size of pixels : %d\n", sizeOfPixels);
    } else if (argc == 4) {
        sizeOfPixels = atoi(argv[2]);
        printf("New size of pixels : %d\n", sizeOfPixels);
        delay = atoi(argv[3]);
        printf("New delay : %d\n", delay);
    }

    pixels = (uint32_t *) malloc(160*144*sizeOfPixels*sizeOfPixels * sizeof(uint32_t));
        
    strcpy(fileToLoad, argv[1]);
    if (access(fileToLoad, F_OK) == -1) {
        SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
            "File not found",
            "The ROM file you provided was not found.",
            NULL);
        exit(EXIT_FAILURE);
    }

    /* Initialisation, création de la fenêtre et du renderer. */
    if(0 != SDL_Init(SDL_INIT_VIDEO)) {
        fprintf(stderr, "Erreur SDL_Init : %s", SDL_GetError());
        stop();
    }
    window = SDL_CreateWindow("GameBoy Emulator", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                              160*sizeOfPixels, 144*sizeOfPixels, SDL_WINDOW_SHOWN | SDL_WINDOW_VULKAN);
    if(NULL == window) {
        fprintf(stderr, "Erreur SDL_CreateWindow : %s", SDL_GetError());
        stop();
    }
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if(NULL == renderer) {
        fprintf(stderr, "Erreur SDL_CreateRenderer : %s", SDL_GetError());
        stop();
    }
    
    if(0 != SDL_RenderClear(renderer)) {
        fprintf(stderr, "Erreur SDL_SetRenderDrawColor : %s", SDL_GetError());
        stop();
    }

    texture = SDL_CreateTexture
            (
            renderer,
            SDL_PIXELFORMAT_ARGB8888,
            SDL_TEXTUREACCESS_STREAMING,
            160*sizeOfPixels, 144*sizeOfPixels
            );

    GameBoy *gameboy = new GameBoy();

    printf("Loading bootrom...\n");
    gameboy->loadBootrom("roms/DMG_ROM.bin");

    printf("Loading program %s...\n", fileToLoad);
    gameboy->setRomToLoad(fileToLoad);
    gameboy->loadProgram(fileToLoad, 0x100, 0x100, 0x200);

    pixels = gameboy->createByteScreen(sizeOfPixels);

    printf("Program read and loaded, starting interpretation...\n");
    std::unique_ptr<DelayCalc> drawDelay(new DelayCalc("draw"));
    int inputs[8];
    memset(inputs, 0, 8*sizeof(int));
    uint32_t currTicks = SDL_GetTicks();

    while (!quit) {
        gameboy->tick(inputs);

        if (gameboy->needsDraw() || gameboy->debugger->isPaused()) {
            SDL_UpdateTexture(texture, NULL, gameboy->getByteScreen(), 160*sizeOfPixels*4);
            SDL_Rect dest = {0,0,160*sizeOfPixels,144*sizeOfPixels};
            SDL_RenderCopy(renderer, texture, nullptr, &dest);
            SDL_RenderPresent(renderer);

            SDL_Event event;
            while (SDL_PollEvent(&event)) {
                switch (event.type) {
                    case SDL_QUIT:
                        quit = 1;
                        break;
                    case SDL_KEYDOWN:
                        switch (event.key.keysym.sym) {
                            case SDLK_a:
                                gameboy->debugger->toggleVerbose();
                                Log::log(true, "Setting verbose : %d\n", gameboy->debugger->getVerbose());
                                break;
                            case SDLK_c:
                                printf("Palette : %02X\n", gameboy->mmu->readByte(0xff47));
                                break;
                            case SDLK_d:
                                gameboy->debugger->printDebug(gameboy->cpu->getPC());
                                break;
                                
                            case SDLK_e:
                                printf("Stat: %02X\n", gameboy->mmu->readByte(0xFF41));
                                break;
                            case SDLK_f:
                                Log::log(true, "FLAGS :\n");
                                Log::log(true, "  Zero : %d\n", gameboy->cpu->getFlag(Flag::Zero));
                                Log::log(true, "  Negative : %d\n", gameboy->cpu->getFlag(Flag::Negative));
                                Log::log(true, "  HalfCarry : %d\n", gameboy->cpu->getFlag(Flag::HalfCarry));
                                Log::log(true, "  Carry : %d\n", gameboy->cpu->getFlag(Flag::Carry));
                                break;
                            case SDLK_i:
                                printf("INTERRUPTS\n");
                                printf("  Master interrupts : %d\n", gameboy->masterInterrupt);
                                printf("  IE : %02X\n", gameboy->mmu->readByte(0xFFFF));
                                printf("  IF : %02X\n", gameboy->mmu->readByte(0xFF0F));
                                break;
                            case SDLK_j:
                                printf("Joypad : %02X\n", gameboy->mmu->readByte(0xFF00));
                                break;
                            case SDLK_l:
                                printf("LY: %02X\n", gameboy->ppu->getLY());
                                break;
                            case SDLK_m:
                                printf("%02X\n", gameboy->mmu->readByte(0));
                                //gameboy->debugger->printMemory(0x0, 0x200);
                                break;
                            case SDLK_o:
                                printf("%02X\n", gameboy->cpu->getOpcode());
                                break;
                            case SDLK_p:
                                gameboy->debugger->togglePause();
                                break;
                            case SDLK_s:
                                gameboy->debugger->toggleStepByStep();
                                Log::log(true, "Setting stepbystep exec : %d\n", gameboy->debugger->isStepByStep());
                                break;
                            case SDLK_v:
                                gameboy->debugger->printVRAM();
                                break;
                            case SDLK_0: {
                                Log::log(true, "REGISTERS :\n");
                                const char* regName[4] = {"AF   :", "BC   :", "DE   :", "HL   :", };
                                uint16_t regValue = *gameboy->cpu->rp2(3);
                                Log::log(true, "  %s 0x%04X\n", regName[0], regValue);
                                regValue = *gameboy->cpu->rp2(0);
                                Log::log(true, "  %s 0x%04X\n", regName[1], regValue);
                                regValue = *gameboy->cpu->rp2(1);
                                Log::log(true, "  %s 0x%04X\n", regName[2], regValue);
                                regValue = *gameboy->cpu->rp2(2);
                                Log::log(true, "  %s 0x%04X\n", regName[3], regValue);
                                break;
                            }
                            case SDLK_1 ... SDLK_8: {
                                uint8_t regValue = *gameboy->cpu->r(event.key.keysym.sym - 49);
                                const char* regName[8] = {"B", "C", "D", "E", "H", "L", "(HL)", "A"};
                                Log::log(true, "  %s: 0x%02X\n", regName[event.key.keysym.sym - 49], regValue);
                                break; 
                            }

                            case SDLK_n:
                                inputs[0] = 1;
                                break;
                            case SDLK_b:
                                inputs[1] = 1;
                                break;
                            case SDLK_LSHIFT:
                                inputs[2] = 1;
                                break;
                            case SDLK_RETURN:
                                inputs[3] = 1;
                                break;
                            case SDLK_RIGHT:
                                inputs[4] = 1;
                                break;
                            case SDLK_LEFT:
                                inputs[5] = 1;
                                break;
                            case SDLK_UP:
                                inputs[6] = 1;
                                break;
                            case SDLK_DOWN:
                                inputs[7] = 1;
                                break;
                        }
                        break;
                    case SDL_KEYUP:
                        switch (event.key.keysym.sym) {
                            case SDLK_n:
                                inputs[0] = 0;
                                break;
                            case SDLK_b:
                                inputs[1] = 0;
                                break;
                            case SDLK_LSHIFT:
                                inputs[2] = 0;
                                break;
                            case SDLK_RETURN:
                                inputs[3] = 0;
                                break;
                            case SDLK_RIGHT:
                                inputs[4] = 0;
                                break;
                            case SDLK_LEFT:
                                inputs[5] = 0;
                                break;
                            case SDLK_UP:
                                inputs[6] = 0;
                                break;
                            case SDLK_DOWN:
                                inputs[7] = 0;
                                break;
                        }
                        break;
                }
            }
            
            // uint32_t newTicks = SDL_GetTicks();
            // uint16_t passedTicks = newTicks - currTicks;
            // if (passedTicks < 16) {
            //     SDL_Delay(16 - passedTicks);
            // }
            
            currTicks = SDL_GetTicks();
        }
    }

    gameboy->saveGame();
    
    exit(0);
    stop();
    return 0;
}

unsigned int *getKeyBuffer() {
    return keyBuffer;
}