#include <cstdint>
#include <cstdio>
#include <cstring>
#include <memory>

#include <cstdlib>

#include "MBC/MBC3.hpp"
#include "MMU.hpp"
#include "GameBoy.hpp"
#include "Debugger.hpp"

MBC3::MBC3(uint8_t cartridgeType, uint8_t romSize, uint8_t ramSize) : MBC(cartridgeType, romSize, ramSize) {
    printf("Instanciated MBC3!\n");
}

void MBC3::handleCalls(uint16_t addr, uint8_t data) {
    if (addr < 0x2000) {
        if ((data & 0xF) == 0xA) {
            if (ramContent == NULL) {
                if (this->ramSize == 0x03) {
                    this->ramContent = (uint8_t *) malloc(sizeof(uint8_t) * 32768);
                    this->ramFileSize = sizeof(uint8_t) * 32768;
                } else {
                    printf("RAM size not supported!\n");
                }
            }

            this->ramEnabled = true;
        } else {
            this->ramEnabled = false;
        }
    } else if (addr >= 0x2000 && addr < 0x4000) {
        int bankId = data & 0b01111111;
        if (bankId == 0x0)
            bankId++;
        this->selectedRomBank = bankId;
        //printf("Changing ROM Bank to %02X\n", bankId);
    } else if (addr >= 0x4000 && addr < 0x6000) {
        if (this->ramContent != NULL) {
            if ((data) <= 0x3) {
                // printf("Changing RAM Bank to %02X\n", data);

                this->selectedRamBank = data;
            } else {
                printf("RTC NOT SUPPORTED\n");
            }
        }
    } else if (addr >= 0x6000 && addr < 0x8000) {
        printf("Latch Clock Data\n");
    }
}

void MBC3::writeByteRam(uint16_t addr, uint8_t data) {
    if (this->ramEnabled) {
        this->ramContent[addr-0xA000 + 0x2000*selectedRamBank] = data;
    }
}

uint8_t MBC3::readByteRam(uint16_t addr) {
    if (this->ramEnabled) {
        return this->ramContent[addr-0xA000 + 0x2000*selectedRamBank];
    } else {
        return 0xFF;
    }
}

uint8_t *MBC3::getPointerToRam(uint16_t addr) {
    return &ramContent[addr-0xA000 + 0x2000*selectedRamBank];
}

uint8_t MBC3::getRomBankNumber() {
    return this->selectedRomBank;
}

void MBC3::restoreRAM(const char *fileName) {
    FILE *saveFile = fopen(fileName, "rb");
    int size = 0;
    fseek(saveFile, 0, SEEK_END);
    size = ftell(saveFile);
    fseek(saveFile, 0, SEEK_SET);

    this->ramContent = (uint8_t *) malloc(sizeof(uint8_t)*size);
    this->ramFileSize = size;

    fread(this->ramContent,sizeof(uint8_t)*size, 1, saveFile);

    fclose(saveFile);
}

void MBC3::saveRAM(const char *fileName) {
    char saveFileName[256];
    strcpy(saveFileName, fileName);
    strcat(saveFileName, ".sav");
    printf("Saving game... %s\n", saveFileName);

    FILE *saveFile = fopen(saveFileName, "wb");
    uint8_t *ramFile = this->ramContent;
    fwrite(ramFile, this->ramFileSize, 1, saveFile);
    fclose(saveFile);
}
