#include <cstdint>
#include <cstdio>
#include <cstring>
#include <memory>
#include <cstdlib>

#include "MBC/MBC2.hpp"
#include "MMU.hpp"
#include "GameBoy.hpp"
#include "Debugger.hpp"

MBC2::MBC2(uint8_t cartridgeType, uint8_t romSize, uint8_t ramSize) : MBC(cartridgeType, romSize, ramSize) {
    printf("Instanciated MBC2!\n");
}

void MBC2::handleCalls(uint16_t addr, uint8_t data) {
    if (addr < 0x4000) {
        if (((addr >> 8) & 0x1) == 0x0) { // This enable or disable RAM
            if (data == 0x0A) {
                if (ramContent == NULL) {
                    if (this->ramSize == 0x00) {
                        this->ramContent = (uint8_t *) malloc(sizeof(uint8_t) * 512);
                        this->ramFileSize = sizeof(uint8_t) * 512;
                        printf("RAM array created\n");
                    } else {
                        printf("RAM size not supported!\n");
                    }
                }
                this->ramEnabled = true;
                //printf("RAM enabled\n");
            } else {
                this->ramEnabled = false;
                //printf("RAM disabled\n");
            }
        } else { // This changes the ROM bank
            int bankId = data & 0xF;
            if (bankId == 0x0) {
                bankId++;
            }
            this->selectedRomBank = bankId;
            //printf("Rom Bank %02X, addr %04X\n", this->selectedRomBank, addr);
        }
    }
}

void MBC2::writeByteRam(uint16_t addr, uint8_t data) {
    if (this->ramEnabled) {
        this->ramContent[addr-0xA000 + 0x2000*selectedRamBank] = data;
    }
}

uint8_t MBC2::readByteRam(uint16_t addr) {
    if (this->ramEnabled) {
        return this->ramContent[addr-0xA000 + 0x2000*selectedRamBank];
    } else {
        return 0xFF;
    }
}

uint8_t *MBC2::getPointerToRam(uint16_t addr) {
    return &ramContent[addr-0xA000 + 0x2000*selectedRamBank];
}

uint8_t MBC2::getRomBankNumber() {
    return this->selectedRomBank;
}

void MBC2::restoreRAM(const char *fileName) {
    FILE *saveFile = fopen(fileName, "rb");
    int size = 0;
    fseek(saveFile, 0, SEEK_END);
    size = ftell(saveFile);
    fseek(saveFile, 0, SEEK_SET);

    this->ramContent = (uint8_t *) malloc(sizeof(uint8_t)*size);
    this->ramFileSize = size;

    fread(this->ramContent,sizeof(uint8_t)*size, 1, saveFile);

    fclose(saveFile);
}

void MBC2::saveRAM(const char *fileName) {
    char saveFileName[256];
    strcpy(saveFileName, fileName);
    strcat(saveFileName, ".sav");
    printf("Saving game... %s\n", saveFileName);

    FILE *saveFile = fopen(saveFileName, "wb");
    uint8_t *ramFile = this->ramContent;
    fwrite(ramFile, this->ramFileSize, 1, saveFile);
    fclose(saveFile);
}
