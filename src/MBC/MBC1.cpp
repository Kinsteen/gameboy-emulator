#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <memory>

#include "Debugger.hpp"
#include "GameBoy.hpp"
#include "MBC/MBC1.hpp"
#include "MMU.hpp"

MBC1::MBC1(uint8_t cartridgeType, uint8_t romSize, uint8_t ramSize) : MBC(cartridgeType, romSize, ramSize) {
    printf("Instanciated MBC1!\n");
}

void MBC1::handleCalls(uint16_t addr, uint8_t data) {
    if (addr < 0x2000) {
        if ((data & 0xF) == 0xA) {
            if (ramContent == NULL) {
                if (this->ramSize == 0x02) {
                    this->ramContent = (uint8_t *)malloc(sizeof(uint8_t) * 8192);
                    this->ramFileSize = sizeof(uint8_t) * 8192;
                    printf("RAM array created\n");
                } else {
                    printf("RAM size (type %02x) not supported!\n", this->ramSize);
                }
            }
            this->ramEnabled = true;
        } else {
            this->ramEnabled = false;
        }
    } else if (addr >= 0x2000 && addr < 0x4000) {
        int bankId = data & 0b00011111;
        if ((bankId & 0x1F) == 0x0) {
            bankId++;
        }
        this->selectedRomBank = bankId;
    } else if (addr >= 0x4000 && addr < 0x6000) {
        int bankId = ((data & 0b11) << 5) | this->selectedRomBank;
        this->selectedRomBank = bankId;
    } else if (addr >= 0x6000 && addr < 0x8000) {
        printf("Tried to change mode to %02X\n", data & 1);
    }
}

void MBC1::writeByteRam(uint16_t addr, uint8_t data) {
    if (this->ramEnabled && this->ramContent != NULL) {
        this->ramContent[addr - 0xA000 + 0x2000 * selectedRamBank] = data;
    }
}

uint8_t MBC1::readByteRam(uint16_t addr) {
    if (this->ramEnabled && this->ramContent != NULL) {
        return this->ramContent[addr - 0xA000 + 0x2000 * selectedRamBank];
    } else {
        return 0xFF;
    }
}

uint8_t *MBC1::getPointerToRam(uint16_t addr) {
    return &ramContent[addr - 0xA000 + 0x2000 * selectedRamBank];
}

uint8_t MBC1::getRomBankNumber() {
    return this->selectedRomBank;
}

void MBC1::restoreRAM(const char *fileName) {
    FILE *saveFile = fopen(fileName, "rb");
    int size = 0;
    fseek(saveFile, 0, SEEK_END);
    size = ftell(saveFile);
    fseek(saveFile, 0, SEEK_SET);

    this->ramContent = (uint8_t *)malloc(sizeof(uint8_t) * size);
    this->ramFileSize = size;

    fread(this->ramContent, sizeof(uint8_t) * size, 1, saveFile);

    fclose(saveFile);
}

void MBC1::saveRAM(const char *fileName) {
    char saveFileName[256];
    strcpy(saveFileName, fileName);
    strcat(saveFileName, ".sav");
    printf("Saving game... %s\n", saveFileName);

    FILE *saveFile = fopen(saveFileName, "wb");
    uint8_t *ramFile = this->ramContent;
    fwrite(ramFile, this->ramFileSize, 1, saveFile);
    fclose(saveFile);
}
