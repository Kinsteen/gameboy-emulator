#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <memory>

#include "MBC/MBC.hpp"

MBC::MBC(uint8_t cartridgeType, uint8_t romSize, uint8_t ramSize) {
    this->cartridgeType = cartridgeType;
    this->romSize = romSize;
    this->ramSize = ramSize;
}

MBC::~MBC() {
    free(this->ramContent);
}
