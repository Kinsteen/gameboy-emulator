#include <cstdint>
#include <cstdlib>
#include <cstdio>
#include <memory>

#include "Interrupt.hpp"
#include "GameBoy.hpp"
#include "CPU.hpp"
#include "MMU.hpp"
#include "Debugger.hpp"
#include "Log.hpp"

Interrupt::Interrupt(GameBoy *gb, CPU *cpu, MMU *mmu) {
    this->cpu = cpu;
    this->gb = gb;
    this->mmu = mmu;
    this->mmu->writeByte(0xFF0F, 0xE0, false, true);
}

Interrupt::~Interrupt()
{
}

void Interrupt::requestInterrupt(enum InterruptType type) {
    uint8_t IF = this->mmu->readByte(0xFF0F); // Interrupt request
    mmu->writeByte(0xFF0F, IF | (1 << type), false, true);
}

int Interrupt::checkInterrupt() {
    uint8_t IF = this->mmu->readByte(0xFF0F); // Interrupt request
    uint8_t IE = this->mmu->readByte(0xFFFF); // Interrupt enable

    for (int i = 0; i < 5; i++) {
        // Bit 0: V-Blank
        // Bit 1: LCD STAT
        // Bit 2: Timer
        // Bit 3: Serial
        // Bit 4: Joypad

        if (((IF >> i) & 0x1) == 1 && ((IE >> i) & 0x1) == 1) {
            if (cpu->cpuHalted) {
                this->cpu->cpuHalted = false;
            } 

            if (gb->masterInterrupt == 1) {
                this->mmu->writeByte(0xFF0F, IF & ~(1<<i), false, true);
                this->cpu->interrupt(0x40 + i*8);
            }
        }
    }

    return 0;
}
