#include <cstdint>
#include <cstdlib>
#include <cstdio>

#include "Debugger.hpp"
#include "MMU.hpp"
#include "Log.hpp"

Debugger::Debugger(MMU *mmu) {
    this->mmu = mmu;
}

Debugger::~Debugger() {}

bool Debugger::addBreakpoint(uint16_t addr) {
    for (size_t i = 0; i < sizeof(this->breakpoints)/sizeof(this->breakpoints[0]); i++) {
        if (this->breakpoints[i] == addr) { // Check if breakpoints isn't already defined
            fprintf(stderr, "This breakpoint already exists !");
            return false;
        }

        if (this->breakpoints[i] == 0x0000) {
            this->breakpoints[i] = addr;
            Log::log(true, "Added breakpoint at index %d, for address %04x\n", (int) i, addr);
            return true;
        }
    }
    
    return false;
}

/**
 * If breakpoint is detected, will return true, else will return false.
 * 
 */
bool Debugger::checkBreakpoints(uint16_t currentAddr) {
    for (size_t i = 0; i < sizeof(this->breakpoints)/sizeof(this->breakpoints[0]); i++) {
        if (this->breakpoints[i] == 0x0000) { // We've cycled through every breakpoints set.
            return false;
        }

        if (currentAddr == this->breakpoints[i]) {
            Log::log(true, "Found breakpoint, pausing at %04x\n", currentAddr);
            return true;
        }
    }

    return false;
}

void Debugger::printDebug(uint16_t programCounter) {
    uint8_t opcode = mmu->readByte(programCounter);
    uint8_t x = opcode >> 6;
    uint8_t y = (opcode >> 3) & 0b111;
    uint8_t z = opcode & 0b111;
    
    Log::log(true, "- - -\n", opcode, x, y, z);

    if (opcode == 0xCB) {
        opcode = mmu->readByte(programCounter + 1);
        x = opcode >> 6;
        y = (opcode >> 3) & 0b111;
        z = opcode & 0b111;
        Log::log(true, "CB - PC : %04x\n", programCounter);
        Log::log(true, "CB - %02x : %d, %d, %d\n", opcode, x, y, z);
    } else {
        Log::log(true, "PC : %04x\n", programCounter);
        Log::log(true, "%02x : %d, %d, %d\n", opcode, x, y, z);
    }
}

void Debugger::printDebug(uint16_t programCounter, bool verbose) {
    uint8_t opcode = mmu->readByte(programCounter);
    uint8_t x = opcode >> 6;
    uint8_t y = (opcode >> 3) & 0b111;
    uint8_t z = opcode & 0b111;

    if (opcode == 0xCB) {
        opcode = mmu->readByte(programCounter + 1);
        x = opcode >> 6;
        y = (opcode >> 3) & 0b111;
        z = opcode & 0b111;
        Log::log(verbose, "CB - PC : %04x\n", programCounter + 1);
        Log::log(verbose, "CB - %02x : %d, %d, %d\n", opcode, x, y, z);
    } else {
        Log::log(verbose, "PC : %04x\n", programCounter);
        Log::log(verbose, "%02x : %d, %d, %d\n", opcode, x, y, z);
    }
}

void Debugger::printOAM() {
    Log::log(true, "---PRINTING OAM---\n");
    for (size_t i = 0xFE00; i < 0xFEA0; i++) {
        if ((i - 0xFE00) % 16 == 0) {
            Log::log(true, "\nOAM %04X: ", i);
        }

        if (mmu->readByte(i) != 0) 
            Log::log(true, "%02X ", mmu->readByte(i));
        else 
            Log::log(true, "   ");
    }
    Log::log(true, "\n---    ENDED    ---\n");
}

void Debugger::printVRAM() {
    Log::log(true, "---PRINTING VRAM---\n");
    for (size_t i = 0x8000; i < 0x9FFF; i++) {
        if ((i - 0x8000) % 16 == 0) {
            Log::log(true, "\nVRAM %04X: ", i);
        }

        if (mmu->readByte(i) != 0) 
            Log::log(true, "%02X ", mmu->readByte(i));
        else 
            Log::log(true, "   ");
    }
    Log::log(true, "\n---    ENDED    ---\n");
}

void Debugger::printMemory(uint16_t start, uint16_t end) {
    Log::log(true, "---PRINTING MEMORY---");
    for (uint16_t i = start; i < (end); i++) {
        if ((i - start) % 16 == 0) {
            Log::log(true, "\nMEM %04X: ", i);
        }

        Log::log(true, "%02X ", mmu->readByte(i));
    }
    Log::log(true, "\n---     ENDED     ---\n");
}


int Debugger::getVerbose() {
    return verbose;
}
bool Debugger::cpuVerbose() {
    return verbose == 1 || verbose == 3;
}
bool Debugger::ppuVerbose() {
    return verbose == 2 || verbose == 3;
}
bool Debugger::otherVerbose() {
    return verbose == 3;
}
bool Debugger::isPaused() {
    return pause;
}
bool Debugger::isStepByStep() {
    return stepByStep;
}

void Debugger::toggleVerbose() {
    int cycle[] = {1, 2, 3, 0};
    this->verbose = cycle[verbose];
}
void Debugger::togglePause() {
    if (this->isPaused()) {
        this->setPause(false);
        this->forceTick = true; // This will force the CPU to execute the instruction, because it'll trigger the breakpoint when unpaused.
    } else {
        this->setPause(true);
    }
}
void Debugger::toggleStepByStep() {
    this->stepByStep = !stepByStep;
}

bool Debugger::getForcetick() {
    return this->forceTick;
}

void Debugger::resetForcetick() {
    this->forceTick = false;
}

void Debugger::setVerbose(bool verbose) {
    this->verbose = verbose;
}
void Debugger::setPause(bool pause) {
    this->pause = pause;
}
void Debugger::setStepByStep(bool stepByStep) {
    this->stepByStep = stepByStep;
}
