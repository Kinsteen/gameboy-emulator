#include <cstdint>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <memory>

#include <SDL2/SDL.h>
#include "GameBoy.hpp"
#include "CPU.hpp"
#include "MMU.hpp"
#include "PPU.hpp"
#include "Log.hpp"
#include "DelayCalc.hpp"
#include "Debugger.hpp"

GameBoy::GameBoy()
{
    printf("GAMEBOY INIT\n");

    this->mmu = new MMU(this);

    this->debugger = new Debugger(this->mmu);

    this->cpu = new CPU(this, this->debugger);

    this->cpu->setMMU(this->mmu);

    this->ppu = new PPU(this, this->cpu, this->mmu, this->debugger);

    this->debugger->setVerbose(false);
    this->debugger->setPause(false);
    this->debugger->setStepByStep(false);

    this->mmu->writeByte(0xFF40, 0x80); // LCD Control Register
    this->mmu->writeByte(0xFF0F, 0xE1); // Interrupts
}

GameBoy::~GameBoy() {}

void GameBoy::printState() {
    Log::log(true, "Actual state : opcode %02X at %04X\n", cpu->getOpcode(), cpu->getPC());
}

void GameBoy::saveGame() {
    mmu->save(this->romToLoad);
}

void GameBoy::setRomToLoad(const char *name) {
    strcpy(this->romToLoad, name);
}

void GameBoy::loadBootrom(const char *fileName) {
    mmu->loadBootrom(fileName);
}

/**
 * This loads the file specified in argument in memory. It can offset the position of memory writing, specify a start and an end to write
 * 
 */
void GameBoy::loadProgram(const char *fileName, int startOffset, int startRead, int size) {
    if (mmu->getRom() == NULL) {
        mmu->loadRomInRAM(fileName);
    }

    char fileSaveName[256];
    
    strcpy(fileSaveName, fileName);
    strcat(fileSaveName, ".sav");
    if (access(fileSaveName, F_OK) == -1) {
        printf("No save file found.\n");
    } else {
        printf("Save found! Loading content... ");
        mmu->restoreRAM(fileSaveName);
        printf("Loaded!\n");
    }
}

/**
 * This function will be called at every frame of the emulator.
 * It's meant to manage the debugging process, to call the CPU to execute next instruction or not.
 */
void GameBoy::tick(int *inputs) {
    // Check if we hit a breakpoint
    // if (!debugger->isPaused() && debugger->checkBreakpoints(cpu->getPC()) && !debugger->getForcetick()) {
    //     debugger->setPause(true);
    //     debugger->printDebug(cpu->getPC(), true); // This will print actual instruction
    // }

    if (!debugger->isPaused() || debugger->getForcetick()) {
        // if (debugger->cpuVerbose()) {
        //     printf("---\n");
        //     debugger->printDebug(cpu->getPC(), true); // This will print actual instruction
        // }
        this->setJoypad(inputs);
        uint8_t cycles = this->cpu->tick();

        for (int i = 0; i < cycles/2; i++) {
            this->ppu->tick(1);
        }
        // With minimal PPU, it takes 4.3s to get out of bootrom.
        // ~7s with drawing.

        // debugger->resetForcetick();
        
        // if (debugger->isStepByStep() && !debugger->isPaused()) {
        //     debugger->setPause(true);
        //     debugger->printDebug(cpu->getPC()); // This will print next instruction
        // }
    } else { // We are paused.
        SDL_Delay(16);
    }
}

uint32_t *GameBoy::createByteScreen(int sizeOfPixels) {
    return this->ppu->createByteScreen(sizeOfPixels);
}

Pixel *GameBoy::getScreen() {
    return this->ppu->getScreen();
}

uint32_t *GameBoy::getByteScreen() {
    return this->ppu->getByteScreen();
}

bool GameBoy::needsDraw() {
    return this->ppu->needsRedraw();
}

void GameBoy::turnOffBootrom() {
    mmu->turnOffBootrom();
    Log::log(true, "Bootrom off.\n");
}

void GameBoy::setJoypad(int *inputs) {
    uint8_t joypad = mmu->readByte(0xFF00);

    if (((joypad >> 4) & 0b11) == 0b10) { // Direction selected
        mmu->writeByte(0xFF00,  (joypad & 0xF0) | 0x0F);
        for (int i = 0; i < 4; i++) {
            if (inputs[i+4] == 1) {
                mmu->writeByte(0xFF00, (joypad & 0xF0) | (~(1 << i) & 0x0F));
            }
        }
    } else if (((joypad >> 4) & 0b11) == 0b01) { // Button selected
        mmu->writeByte(0xFF00,  (joypad & 0xF0) | 0x0F);
        for (int i = 0; i < 4; i++) {
            if (inputs[i] == 1) {
                mmu->writeByte(0xFF00, (joypad & 0xF0) | (~(1 << i) & 0x0F));
            }
        }
    } else if (((joypad >> 4) & 0b11) == 0b11) {
        mmu->writeByte(0xFF00,  0xFF);
    } else if (((joypad >> 4) & 0b11) == 0b00) {
        mmu->writeByte(0xFF00,  0xCF);
    }
}
