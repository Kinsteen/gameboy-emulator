#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <memory>

#include "CPU.hpp"
#include "Debugger.hpp"
#include "DelayCalc.hpp"
#include "GameBoy.hpp"
#include "Interrupt.hpp"
#include "Log.hpp"
#include "MMU.hpp"
#include "PPU.hpp"
#include "main.hpp"

PPU::PPU(GameBoy *gb, CPU *cpu, MMU *mmu, Debugger *debugger) {
    this->gb = gb;
    this->cpu = cpu;
    this->mmu = mmu;
    this->debugger = debugger;
    this->ticks = 0;
    this->state = VBlank;
    this->LY = 0x91;
    this->displayEnable = true;
    Pixel w;
    w.color = Black;
    for (size_t i = 0; i < 160 * 144; i++) {
        this->screen[i] = w;
    }
}

PPU::~PPU() {
}

void PPU::tick(unsigned int cycles) {
    if (this->readControl(7) == 0) {
        if (displayEnable == true) {
            turnOffDisplay();
            this->redraw = true;
        }
        return; // Quit if display is off
    }

    if (this->readControl(7) == 1 && displayEnable == false) {
        displayEnable = true;
    }

    bool intRequest = 0;

    //printf("%d\n", this->ticks);

    switch (this->state) {
        case OAMSearch: {
            // In this state, the PPU would scan the OAM (Objects Attribute Memory)
            // from 0xfe00 to 0xfe9f to mix sprite pixels in the current line later.
            // This always takes 80 ticks.

            if (this->ticks == 0) {
                this->spritesIndex = 0;
                this->tilesReadInLine = 0;
            }

            if (readStat(5) == 0x1) {
                intRequest = 1;
            }

            // Load all sprites in array
            if (this->ticks < 40) {
                int s = this->ticks;
                int spriteAddr = 0xFE00 + s * 4;

                int sY = mmu->readByte(spriteAddr, true) - 16;    // -16 because (0,0) is hidden sprite
                int sX = mmu->readByte(spriteAddr + 1, true) - 8; // Same
                int sTile = mmu->readByte(spriteAddr + 2, true);
                if (readControl(2)) {
                    sTile = sTile & 0xFE;
                }
                int sAttr = mmu->readByte(spriteAddr + 3, true);

                bool sPrio = sAttr >> 7 & 1;
                bool sYFlip = sAttr >> 6 & 1;
                bool sXFlip = sAttr >> 5 & 1;
                uint8_t sPalette = sAttr >> 4 & 1;

                uint16_t address = 0x8000 + (sTile << 4); // All tile data starts at 8000

                struct Sprite spriteToAdd;

                if (sX > -8 && sX < 160 && sY > -16 && sY < 144) {
                    spriteToAdd.x = sX;
                    spriteToAdd.y = sY;
                    spriteToAdd.tileNumber = sTile;
                    spriteToAdd.prio = sPrio;
                    spriteToAdd.XFlip = sXFlip;
                    spriteToAdd.YFlip = sYFlip;
                    if (sPalette == 0)
                        spriteToAdd.paletteData = mmu->readByte(0xFF48);
                    else
                        spriteToAdd.paletteData = mmu->readByte(0xFF49);
                    spriteToAdd.dataAddr = address;

                    frameSprites[spritesIndex++] = spriteToAdd;
                }
            }

            if (this->ticks >= 80) {
                this->x = 0;
                this->state = PixelTransfer;
                uint8_t stat = this->mmu->readByte(0xFF41);
                this->mmu->writeByte(0xFF41, (stat & 0xFC) | 3);
                Log::log(debugger->ppuVerbose(), "Switching to PixelTransfer...\n");
            }

            this->ticks++;
            break;
        }
        case PixelTransfer: {
            uint16_t tileBgMap = this->readControl(3) == 1 ? 0x9C00 : 0x9800;
            uint8_t palette;

            bool is816 = this->readControl(2);

            int y = this->LY;

            bool isSprite = false;

            Pixel px;
            uint8_t bgData = 0;
            uint8_t spriteData;

            palette = mmu->readByte(0xFF47);

            if (this->readControl(0) == 1) {
                this->scrollX = mmu->readByte(0xFF43);
                this->scrollY = mmu->readByte(0xFF42);

                uint8_t tileX = (x + scrollX) % 256 / 8;
                uint8_t tileY = (y + scrollY) % 256 / 8;
                uint8_t tileID = mmu->readByte(tileBgMap + tileY * 32 + tileX, true);
                uint16_t address;
                if (this->readControl(4) == 1) {
                    address = 0x8000 + (tileID << 4);
                } else {
                    if (tileID < 0x80) {
                        address = 0x9000 + (tileID << 4);
                    } else {
                        address = 0x8000 + (tileID << 4);
                    }
                }

                uint8_t line = (y + scrollY) % 8;
                uint16_t rawData = mmu->readWord(address + line * 2, true);
                uint8_t rawData1 = rawData & 0x00FF;
                uint8_t rawData2 = (rawData & 0xFF00) >> 8;

                int8_t b = 7 - (x + scrollX) % 8; // Little endian again.
                bgData = ((rawData2 >> b << 1) & 0b10) | ((rawData1 >> b) & 0b01);
            }

            if (this->readControl(5) == 1 && this->readControl(0) == 1) {          // Window enable
                uint16_t winTileMap = this->readControl(6) == 0 ? 0x9800 : 0x9C00; // to 9FFF and 9BFF respectively

                int winYPos = mmu->readByte(0xFF4A);
                int winXPos = mmu->readByte(0xFF4B) - 7;

                if (WILC == -1) {
                    if (x >= winXPos && y >= winYPos) {
                        WILC = y;
                    }
                }

                if (x >= winXPos && WILC >= winYPos && WILC != -1) {
                    uint8_t tileX = (x - winXPos) / 8;
                    uint8_t tileY = (WILC - winYPos) / 8;
                    uint8_t tileID = mmu->readByte(winTileMap + tileY * 32 + tileX, true);
                    uint16_t address;
                    if (this->readControl(4) == 1) {
                        address = 0x8000 + (tileID << 4);
                    } else {
                        if (tileID < 0x80) {
                            address = 0x9000 + (tileID << 4);
                        } else {
                            address = 0x8000 + (tileID << 4);
                        }
                    }
                    uint8_t line = (WILC - winYPos) % 8;
                    uint16_t rawData = mmu->readWord(address + line * 2, true);
                    uint8_t rawData1 = rawData & 0x00FF;
                    uint8_t rawData2 = (rawData & 0xFF00) >> 8;

                    int8_t b = 7 - ((x - winXPos) % 8); // Little endian again.
                    bgData = ((rawData2 >> b << 1) & 0b10) | ((rawData1 >> b) & 0b01);
                    drewWindow = true;
                }
            }

            if (this->readControl(1) == 1) {                                      // Sprite enable
                uint8_t spriteXs[20];
                uint8_t idx = 0;

                for (int s = spritesIndex - 1; s >= 0 && tilesReadInLine <= 10; s--) { // Cycle through sprites
                    struct Sprite spriteToPrint = frameSprites[s];

                    if (spriteToPrint.prio == 0 || (spriteToPrint.prio == 1 && bgData == 0)) {
                        if ((y >= spriteToPrint.y && y < spriteToPrint.y + 8) || (y >= spriteToPrint.y && y < spriteToPrint.y + 16 && is816)) {
                            if (x >= spriteToPrint.x && x < spriteToPrint.x + 8) {
                                bool canDraw = true;
                                for (int s2 = 0; s2 < idx; s2++) {
                                    // OAM priority
                                    if (spriteToPrint.x == spriteXs[s2]) {
                                        canDraw = false;
                                    }
                                }

                                if (canDraw) {
                                    if (x == spriteToPrint.x) {
                                        tilesReadInLine++;
                                    }
                                    uint8_t line = y - spriteToPrint.y;
                                    if (spriteToPrint.YFlip == 1) {
                                        if (is816) {
                                            line = 15 - line;
                                        } else {
                                            line = 7 - line;
                                        }
                                    }

                                    uint16_t rawData = mmu->readWord(spriteToPrint.dataAddr + line * 2, true); // Line will work with 816 mode ?
                                    uint8_t rawData1 = rawData & 0x00FF;
                                    uint8_t rawData2 = (rawData & 0xFF00) >> 8;

                                    uint8_t b = (x - spriteToPrint.x);
                                    if (spriteToPrint.XFlip == 0) {
                                        b = 7 - b;
                                    }

                                    uint8_t tempSpriteData = ((rawData2 >> b << 1) & 0b10) | ((rawData1 >> b) & 0b01);

                                    if (tempSpriteData != 0) { // If it's not transparent, write it
                                        spriteXs[idx++] = spriteToPrint.x;
                                        isSprite = true;
                                        palette = spriteToPrint.paletteData;
                                        spriteData = tempSpriteData;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            uint8_t finalData = isSprite ? spriteData : bgData;
            px.raw_color = finalData;

            px.color = static_cast<Color>((palette >> (finalData * 2)) & 0b11);
            this->pushPixel(x, y, px);

            this->x += cycles;

            if (this->x >= 160) {
                this->state = HBlank;

                uint8_t stat = this->mmu->readByte(0xFF41);
                this->mmu->writeByte(0xFF41, (stat & 0xFC) | 0);
                Log::log(debugger->ppuVerbose(), "Switching to HBlank...\n");
            }

            this->ticks++;
            break;
        }
        case HBlank: {
            if (readStat(3) == 0x1) {
                intRequest = 1;
            }
            // A full scanline takes 456 ticks to complete. At the end of a
            // scanline, the PPU goes back to the initial OAM Search state.
            // When we reach line 144, we switch to VBlank state instead.

            if (this->ticks >= 456) {
                this->ticks = this->ticks % 456;
                
                if (drewWindow)
                    WILC++;

                drewWindow = false;
                this->LY++;
                mmu->writeByte(0xFF44, this->LY);
                if (this->LY >= 144) {
                    this->state = VBlank;

                    this->WILC = -1; // Reset internal Window line counter

                    this->cpu->getInterruptHandler()->requestInterrupt(IntVBlank);

                    uint8_t stat = this->mmu->readByte(0xFF41);
                    this->mmu->writeByte(0xFF41, (stat & 0xFC) | 1);
                    Log::log(debugger->ppuVerbose(), "Switching to VBlank...\n");
                } else {
                    this->state = OAMSearch;
                    uint8_t stat = this->mmu->readByte(0xFF41);
                    this->mmu->writeByte(0xFF41, (stat & 0xFC) | 2);
                    Log::log(debugger->ppuVerbose(), "Switching to OAMSearch...\n");
                }
            } else {
                this->ticks++;
            }
            break;
        }
        case VBlank: {
            if (readStat(4) == 0x1 || readStat(5) == 0x1) {
                intRequest = 1;
            }
            // Wait ten more scanlines before starting over.
            if (this->ticks >= 456) {
                this->ticks = this->ticks % 456;
                this->LY++;
                if (this->LY >= 154) {
                    this->redraw = true;

                    this->LY = 0;
                    mmu->writeByte(0xFF44, this->LY);
                    this->state = OAMSearch;

                    uint8_t stat = this->mmu->readByte(0xFF41);
                    this->mmu->writeByte(0xFF41, (stat & 0xFC) | 2);
                    Log::log(debugger->ppuVerbose(), "Switching to OAMSearch...\n");
                } else {
                    mmu->writeByte(0xFF44, this->LY);
                }
            } else {
                this->ticks++;
            }
            break;
        }
    }

    uint8_t LY = mmu->readByte(0xFF44);
    uint8_t LYC = mmu->readByte(0xFF45);
    uint8_t stat = this->mmu->readByte(0xFF41);
    if (LY == LYC) {
        mmu->writeByte(0xFF41, stat | (1 << 2), false, true);

        if (readStat(6) == 0x1) {
            intRequest = 1;
        }
    } else {
        mmu->writeByte(0xFF41, stat & ~(1 << 2), false, true);
    }

    if (intRequest == 1) {
        if (oldIntState == 0) {
            // Rising edge!
            this->cpu->getInterruptHandler()->requestInterrupt(IntLCDStat);
        }
    }

    oldIntState = intRequest;
}

void PPU::turnOffDisplay() {
    if (displayEnable) {
        // Disable screen, so reset LY etc
        this->displayEnable = false;
        this->LY = 153;
        this->state = VBlank;
        this->ticks = 456;
        uint8_t stat = this->mmu->readByte(0xFF41);
        memset(this->byteScreen, 0xFFFFFFFF, 160 * 144 * this->sizeOfPixels * this->sizeOfPixels);
        this->mmu->writeByte(0xFF41, (stat & 0xFC) | 0);
    }
}

void PPU::pushPixel(int x, int y, Pixel pixel) {
    int s = this->sizeOfPixels;
    this->screen[x + y * 160] = pixel;

    for (int j = 0; j < s; j++) {
        int index = x * s + y * s * 160 * s + j * 160 * s;
        for (int i = 0; i < s; i++) {
            if (x >= 155 && y >= 139 && this->debugger->isPaused()) { // Show small square if execution is paused
                this->byteScreen[index + i] = 0xFFFF0000;
            } else {
                switch (pixel.color) {
                    case White:
                        this->byteScreen[index + i] = 0xFFFFFFFF;
                        break;
                    case Light:
                        this->byteScreen[index + i] = 0xFFAAAAAA;
                        break;
                    case Dark:
                        this->byteScreen[index + i] = 0xFF555555;
                        break;
                    case Black:
                        this->byteScreen[index + i] = 0xFF000000;
                        break;
                    case Errcolor:
                        this->byteScreen[index + i] = 0xFFFF0000;
                        break;
                }
            }
        }
    }
}

uint8_t PPU::readControl(uint8_t bit) {
    uint8_t LCDC = mmu->readByte(0xFF40);

    return LCDC >> bit & 1;
    // 7:LCD Display Enable             (0=Off, 1=On)
    // 6:Window Tile Map Display Select (0=9800-9BFF, 1=9C00-9FFF)
    // 5:Window Display Enable          (0=Off, 1=On)
    // 4:BG & Window Tile Data Select   (0=8800-97FF, 1=8000-8FFF)
    // 3:BG Tile Map Display Select     (0=9800-9BFF, 1=9C00-9FFF)
    // 2:OBJ (Sprite) Size              (0=8x8, 1=8x16)
    // 1:OBJ (Sprite) Display Enable    (0=Off, 1=On)
    // 0:BG/Window Display/Priority     (0=Off, 1=On)
}

// Byte 0xFF41
uint8_t PPU::readStat(uint8_t bit) {
    uint8_t stat = mmu->readByte(0xFF41);

    return stat >> bit & 1;
    // Bit 6 - LYC=LY Coincidence Interrupt (1=Enable) (Read/Write)
    // Bit 5 - Mode 2 OAM Interrupt         (1=Enable) (Read/Write)
    // Bit 4 - Mode 1 V-Blank Interrupt     (1=Enable) (Read/Write)
    // Bit 3 - Mode 0 H-Blank Interrupt     (1=Enable) (Read/Write)
    // Bit 2 - Coincidence Flag  (0:LYC<>LY, 1:LYC=LY) (Read Only)
    // Bit 1-0 - Mode Flag       (Mode 0-3, see below) (Read Only)
    //         0: During H-Blank
    //         1: During V-Blank
    //         2: During Searching OAM
    //         3: During Transferring Data to LCD Driver
}

uint32_t *PPU::createByteScreen(int sizeOfPixels) {
    this->byteScreen = (uint32_t *)malloc(160 * 145 * sizeOfPixels * sizeOfPixels * sizeof(uint32_t));
    this->sizeOfPixels = sizeOfPixels;
    return this->byteScreen;
}

State PPU::getState() {
    return this->state;
}

Pixel *PPU::getScreen() {
    return this->screen;
}

uint32_t *PPU::getByteScreen() {
    return this->byteScreen;
}

bool PPU::needsRedraw() {
    bool r = this->redraw;
    this->redraw = false;
    return r;
}

int PPU::getLY() {
    return this->LY;
}

int PPU::getInterrupts() {
    bool temp = VBlankInt;
    VBlankInt = false;
    return temp ? 1 : 0;
}