#include <stdarg.h>
#include <stdio.h>
#include <cstdint>
#include <memory>

#include "Log.hpp"
#include "GameBoy.hpp"

Log::Log() {}

Log::~Log() {}

void Log::log(bool condition, const char* message, ...) {
    if (condition) {
        va_list arg;

        va_start(arg, message);
        vfprintf(stdout, message, arg);
        va_end(arg);
    }
}

void Log::log(const char* message, ...) {
    va_list arg;

    va_start(arg, message);
    vfprintf(stdout, message, arg);
    va_end(arg);
}
