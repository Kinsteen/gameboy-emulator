# GameBoy Emulator
This is the start of an GameBoy emulator, written in C++, with SDL2 as the graphical interface. We have a long way to go before a game could be playable.

The final goal is to be able to play Pokémon Red. First, we'll start to emulate the [bootrom](https://gbdev.gg8.se/wiki/articles/Gameboy_Bootstrap_ROM), because it uses some graphics, and is really simple. Then, Tetris is a good target to emulate, as it does not use memory banks controllers and is quite small. Finally, we could directly try to emulate Pokémon.

## Progression
*Check file history to have progression dated*

Currently, this emulator runs the DMG Bootstrap ROM without problem, runs Tetris entirely. Next step was Dr. Mario, which worked nicely with few little modifications. There are still some quirks, and that'll be ironed out when I'll finish to implement some things.

In terms of graphics, background, sprites and now windows are supported! It's a really ugly implementation, but it works without a really big performance hit (but could be better, maybe 10fps ?). Some MBC are supported (MBC1, 2 and 3), but it is quite barebone. The architecture I came up with is really clumsy, I need to refactor things to make it better. With that, Pokémon Red is totally playable, from start to finish *(probably)*, and so **I've met the goal that I had fixed myself with!**

Everything that I do now is pure bonus, and I'll try to implement some other games, with other MBC, just to iron every aspect of what is coded currently.

## Playable games (tested)
 + **POKÉMON RED**
 + Tetris
 + Dr. Mario
 + The Legend of Zelda: Link's Awakening
 + F-1 Race *(heavy use of scrolling while drawing frame)*
 + Super Mario *(use of Stat interrupt for scrolling)*

## TODO
  + Change the CPU class architecture :
    + Why ? Performance is okay at best, but the readability is really bad.
    + Pass all of instructions calculations in another class, maybe static ? to improve readability.
    + Add functionnality bounded to CPU that's missing, or offloaded to other classes (like DMA OAM transfer, etc) to be more cycle accurate.
  + Improve performance of PPU, and especially sprite drawing. Maybe implement a pixel FIFO ? As of today, a lot of heavy calculations are done pixel per pixel basis, so nearly every cycle. Need to find a way to avoid that (maybe), or at least change how we retrieve each pixel from sprite and BG (and window soon).
  + Finish implementing interrupts (in order : Joypad)
  + Implement HALT and STOP properly

## Debugging
*This is mainly to debug opcode implementation, and reading some memory. It's not meant to debug ROMs.*

With the window in focus, there is multiple keys you can press to trigger a console output.

 + `A key` : toggle debugging output (it'll just flood the console).
 + `C key` : prints BG palette.
 + `D key` : print the actual state of the CPU (program counter, opcode read).
 + `F key` : print the state of the actual flags, in register F.
 + `I key` : print the state of the interrupts registers (IME, IE and IF).
 + `L key` : print LY status.
 + `M key` : print some memory portion around the program counter.
 + `P key` : pause key. If pressed, will pause and resume program. If step-by-step is enabled, will instead be a trace function.
 + `S key` : toggle step-by-step instruction. Be careful, the instructions printed can be offset by 1.
 + `V key` : print all of VRAM (`0x8000` - `0x9FFF`).
 + `0 key` : print all registers in the order `AF, BC, DE, HL`.
 + `Num line (1-8)` : print register in order defined the line before.

## Blargg's tests
Passing :
  + CPU Instrs :
    + 01-special
    + 02-interrupts
    + 03-op sp,hl
    + 04 op r,imm
    + 05-op rp
    + 06-ld r,r
    + 07-jr,jp,call,ret,rst
    + 08-misc instrs
    + 09-op r,r
    + 10-bit ops
    + 11-op a,(hl)

### Bad opcodes
 + None!

# References
 + https://gbdev.io/pandocs/ : obviously ; the St Graal of every emulator dev. Some portions are not so explicated, but it's a great place to find something fast.
 + https://github.com/gbdev/awesome-gbdev#emulator-development : to have some documentation, some open-source emulators.
 + https://gb-archive.github.io/salvage/decoding_gbz80_opcodes/Decoding%20Gamboy%20Z80%20Opcodes.html : how to decode opcodes programatically, to avoid a 500+ case switch statement (we'll still have more than a thousand lines for the CPU, and it is unreadable, but it's interesting.).
 + http://www.cochoy.fr/gb-doc/gameboy-opcodes.html : every opcodes for the GameBoy.
 + http://marc.rawer.de/Gameboy/Docs/GBCPUman.pdf : explications of some opcodes.