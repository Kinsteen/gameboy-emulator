CC = clang++
SRC_EXT = cpp

SRC_DIR = src
INC_DIRS = inc # Can put multiple folder separated by spaces
OUT_DIR = out
BUILD_DIR = build

EXEC_NAME = gbemu
ARGS = roms/Pokemon.gb

CFLAGS = -Wall -std=c++11 -O3 -g -pg
INCLUDES = $(addprefix -I,$(INC_DIRS))
LIBS = -lSDL2main -lSDL2

# Do not touch further

EXEC = $(BUILD_DIR)/$(EXEC_NAME)
SRCS = $(shell find $(SRC_DIR) -type f -name '*.$(SRC_EXT)' -not -name '_*') # Exclude sources files that starts with _
OBJS = $(SRCS:$(SRC_DIR)/%.$(SRC_EXT)=$(OUT_DIR)/%.o)
DEPS = $(OBJS:.o=.d)

$(EXEC): $(OBJS)
	@mkdir -p $(BUILD_DIR)
	$(CC) $(OBJS) $(LIBS) -o $@ -g -pg

# Autobuild dependencies
$(OUT_DIR)/%.o: $(SRC_DIR)/%.$(SRC_EXT)
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $(INCLUDES) -MP -MMD -c $< -o $@

# This line will combine with the one just above, to include
# every dependencies
-include $(DEPS)

.PHONY: test clean mrproper re run bench

test: $(EXEC)
	mangohud --dlsym ./$(EXEC) $(ARGS)

clean:
	@rm -rf $(OUT_DIR)

mrproper: clean
	@rm -rf $(EXEC)

re: mrproper $(EXEC)

run: $(EXEC)
	./$(EXEC) $(ARGS)

bench: $(EXEC)
	mangohud --dlsym make run
